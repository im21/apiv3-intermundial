# API InterMundial V3

## Introducción

A continuación se definen todas las posibles acciones que se pueden realizar a través de version 3 de la API de InterMundial.
Todas las pruebas de la documentación han sido probadas y validadas antes de ser documentadas y explicadas en este documento.

Antes de realizar un reporte sobre un posible error, se ruega realicen las pruebas con la información aqui publicada.

## Consideraciones previas

En esta documentación se pueden encontrar ejemplos funcionales de llamadas a nuestra API. Todos ellos se realizan con el usuario de pruebas:

>**Datos de acceso**
> 
>`Usuario: autobox@intermundial.es`
>
>`Contraseña: 1234567a`

### Sustitución de variables

En los ejemplos existe información que se espera y debe ser proporcionada por el cliente que consuma la API. Dichos campos, a partir de este instante estarán representados como `{valor_a_sustituir}`. Si el campo en cuestión va precedido por una interrogante `{?valor_a_sustituir}` se considerara un valor opcional.

### Formatos de salida

La API de InterMundial soporta dos formatos de entrada y salida, XML y JSON. Debido a su menor peso y facilidad de uso
InterMundial recomienda utilizar JSON.

Si se indica al final de una URL `.json` o `.xml` se forzara el formato de entrada y de salida y prevalecera sobre los cabeceras de formato `Content-Type`. Tambien se puede indicar el formato esperado y de salida con las cabeceras `Content-Type: application/json` o `Content-Type: application/xml`. Es importante que se especifique la cabecera `Accept` para que el servidor detecte bien los formatos de entrada y salida.

### Ejemplos de las llamadas

Todos los ejemplos de la API han sido documentados con ejemplos en [CURL](https://es.wikipedia.org/wiki/CURL)

### Autenticación

Si despues de realizar el login, se desea autenticar y autorizar cualquier llamada sucesiva a la api. Esta acción se puede realizar por cookie o por token.
Si se quiere realizar por medio de token, toda la información necesaria para dicho procedimiento se encuentra en este [enlace](methods/auth/auth_token_usage.md).

### Métodos

**Sesion**
* [Iniciar sesion](methods/users/login.md)
* [Obtener datos de la sesion](methods/users/get_sesion.md)
* [Cerrar sesion](methods/users/delete_sesion.md)
* [Solicitar reseteo de contraseña](methods/users/get_reset_password.md)
* [Cambiar contraseña](methods/users/put_change_password.md)

**Polizas**
* [Listado de pólizas contratables](methods/policies/get_available_policies.md)
* [Detalle de una poliza](methods/policies/get_policy_detail.md)
* [Opciones de configuración de una póliza](methods/policies/get_policy_config_options.md)
* [Obtener precios de una póliza](methods/policies/get_price.md)
* [Obtener el listado de documentos de una poliza](methods/policies/get_policy_documents.md)
* [Descargar un documento](methods/policies/get_policy_document_detail.md)

**Seguros**
* [Listado de seguros contratados](methods/insurances/get_insurances.md)
* [Detalle de un seguro contratado](methods/insurances/get_insurance_detail.md)
* [Contratar un seguro](methods/insurances/post_insurance.md)
* [Modificación de un seguro](methods/insurances/put_insurance.md)
* [Anular un seguro](methods/insurances/delete_insurance.md)
* [Recuperar el certificado de un seguro](methods/insurances/get_certificate.md)

**Presupuestos**
* [Crear un presupuesto](methods/quotes/post_quote.md)
* [Contratar un presupuesto](methods/quotes/post_quote.md)

**VOZ IP**
* [Quiero que me llamen](methods/voip/post_call.md)

**Paises**
* [Listado de paises](methods/countries/get_countries.md)
* [Detalle de un pais](methods/countries/get_country.md)

**Flujos completos y explicados**
* [Tarificar una póliza](examples/rate_policies.md)
* [Tarificar una póliza de porcentaje](examples/rate_policie_porcentaje.md)

**Validaciones de campos**
* [Contratar un seguro](validaciones/post_seguro.md)
