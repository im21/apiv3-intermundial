### Flujo de tarificación de una póliza de tipo porcentaje

Para poder realizar el proceso de tarifación para una póliza con una tarifa de tipo PORCENTAJE, vamos a elegir previamente una póliza. Para este ejemplo usaremos la póliza con id `19223`

**Obteniendo la estructura de la tarifa**

En primer lugar tenemos que obtener las estructura de la tarifa de la póliza. Para ello hacemos la siguiente llamada:

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/es/polizas/19223/opciones
```
<br />
  
**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/polizas/19223/opciones \
  -H 'cache-control: no-cache'
```
<br />

**Respuesta**

El primer campo que tenemos que destacar se encuentra al final del JSON de la llamada `tipo_tarifa`.
Para este ejemplo aparecerá en el campo `tipo_tarifa`, el valor `porcentaje`.

El tipo de tarifa `porcentaje` es la única tarifa distinta al resto tipos de tarifa conocidos. Por tanto, el proceso de tarificación es un poco diferente. 

Se puede observar como se devuelve un arreglo de parametros en el campo `opciones` que esta formado en este caso por dos objetos en la propiedad `valores`.

Para poder calcular el precio tenemos que saber tanto el id de las opciones y el importe máximo asegurado, que en este ejemplo sería desde 0 hasta 20000 €. Los marcare en la respuesta con flechas `-->`

```json
{
    "opciones":[  
      {  
-->      "id":"3630",
         "numero_parametro":0,
         "nombre":"Valor del viaje",
         "tipo":"importe_asegurado_valor",
         "valores":[  
            {  
               "desde":"0",
               "hasta":"10000",
               "id":"00000004410000000001",
               "nombre":"De 0 a 10000",
               "procentaje":0.00942063
            },
            {  
               "desde":"10000.01",
-->            "hasta":"20000",
               "id":"00000004410000000002",
               "nombre":"De 10000.01 a 20000",
               "procentaje":0.00962357
            }
         ]
      }
   ],
-->"tipo_tarifa": "PORCENTAJE"
}
```

**Obtener el precio de una póliza**

Con toda la información anterior ya estamos preparados para obtener el precio de nuestra poliza `19223`.
Tenemos un parametro denominado `importe_asegurado_valor` que indica el importe que queremos asegurar.

Realizamos la llamada:

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/es/polizas/19223/precios?parametros[0][id_parametro]=3630&parametros[0][importe_asegurado_valor]=1000
```
<br />
  
**Solicitud CURL**

```
curl -X GET \
  'https://api.intermundial.com/v3/es/polizas/19223/precios?parametros[0][id_parametro]=3630&parametros[0][importe_asegurado_valor]=1000'
  -H 'cache-control: no-cache'
```
<br />

**Respuesta**

En la respuesta se nos devuelve el `precio_base` del seguro y un flag que siempre tendremos que tener en cuenta `is_tarifa_por_asegurado`. Nos indica si debemos multiplicar los importes por el numero de pasajeros o no.

* No se pueden ampliar las coberturas de la póliza.
* La configuración seleccionada no nos permite ampliar las coberturas.

La formula final quedaría así:

* `precio_base` = `10`

Con esto ya hemos tarificado el seguro. Los datos que se han  seleccionado con la `-->` se requeriran en la llamada de contratar un seguro.

Una vez realizado todo el proceso de la tarificación, vamos a contratar un seguro. Por tanto, a continuación se indica el cuerpo que tenemos que enviar para este ejemplo:

**Endpoint de la llamada **
```
POST https://api.intermundial.com/v3/{locale}/seguros.{?format}
```
<br />


**Cuerpo de la solicitud**

```json
{
   "id":null,
   "referencia_agencia":"PRUEBA",
   "precio_base":20,
   "precio_ampliaciones":0,
   "precio_total":20,
   "poliza":{
      "id":"19223",
      "nombre_comercial":"PORCENTAJE",
      "tipo_tarifa":"PORCENTAJE"
   },
   "asegurados":[
      {
         "ref_agencia":"",
         "tratamiento":"Sr.",
         "nombre":"John",
         "apellidos":"Doe",
         "tipo_documento":"NIF",
         "numero_documento":"35995538D",
         "fecha_nacimiento":"1980-02-20",
         "email":"john.doe@test.com",
         "direccion":"Calle Real",
         "localidad":"Madrid",
         "provincia":"Madrid",
         "pais":{
            "id":305,
            "nombre":"Chipre",
            "continente":"Eastern Europe",
            "codigo_pais":"CYP",
            "text":"Chipre"
         },
         "codigo_postal":"7730",
         "telefono":"663399336",
         "is_principal":true
      },
      {
         "tratamiento":"Sr.",
         "nombre":"Ryan",
         "apellidos":"Cooper",
         "tipo_documento":"NIF",
         "numero_documento":"84220231G",
         "fecha_nacimiento":"1982-04-24",
         "email":"ryan.cooper@test.com",
         "is_principal":false
      }
   ],
   "coberturas_ampliadas":[

   ],
   "fecha_inicio":"2018-06-23",
   "fecha_fin":"2018-06-24",
   "total_asegurados":"2",
   "pais_origen":{
      "id":317,
      "nombre":"España",
      "continente":"Southern Europe",
      "capital":"Madrid",
      "codigo_pais":"ESP"
   },
   "pais_destino":{
      "id":307,
      "nombre":"Alemania",
      "continente":"Western Europe",
      "capital":"Berlin",
      "codigo_pais":"DEU",
      "text":"Alemania"
   },
   "parametros":[
      {
         "id_parametro":"3630",
         "importe_asegurado_valor":"1000"
      }
   ]
}
```


