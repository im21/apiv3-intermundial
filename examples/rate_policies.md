### Flujo de tarificación de una póliza

Aqui vamos a explicar como poder tarificar una póliza por medio de la API. Pero antes de inciar la explicación del flujo,
es apropiado tener en cuenta varias cosas.

* Las polizas pueden ser de tres tipos: tarifa por asegurado, tarifa por reserva y tarifa mixta.
> * En la tarifa por asegurado se multiplican los importes por el numero de pasajeros.
> * En la tarifa por reserva independientemente del numero de pasajeros, los importes no se multiplican.
> * Las tarifas mixtas combinan las dos anteriores.
* La contratación esta estrechamente relacionada con el proceso de tarificación. En la creación de un seguro se valida también la información de la tarifa.
* No hay un patron expecífico para tarificar las pólizas debido a que la estructura y configuración de cada una de ellas puede resultar diferente a otra.

Para poder realizar este ejemplo y que quede bien claro vamos a elegir previamente una póliza. Para este ejemplo usaremos la póliza con id `13700`

**Obteniendo la estructura de la tarifa**

En primer lugar tenemos que obtener las estructura de la tarifa de la póliza. Para ello hacemos la siguiente llamada:

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/es/polizas/13700/opciones
```
<br />
  
**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/polizas/13700/opciones \
  -H 'cache-control: no-cache'
```
<br />

**Respuesta**

El primer campo que tenemos que destacar se encuentra al final del JSON de la llamada `tipo_tarifa`.
El tipo de tarifa es una manera de categorizar las tarifas según su combinación de parametros, existen nueve tipos de tarifa conocidos.
 
* DESTINO_DURACION
* MODALIDAD_IMPORTE
* DURACION_IMPORTE
* DURACION_EDAD
* DESTINO_IMPORTE
* MODALIDAD_DESTINO
* PORCENTAJE
* IMPORTE
* PRIMAFIJA

Se puede observar como se devuelve un arreglo de parametros en el campo `opciones` que esta formado en este caso por dos objetos de parametro.
Cada parametro tiene un listado de los posibles valores que pueden configurarse para la póliza 13700.

Para poder calcular el precio tenemos que saber tanto el id del/los parametros como el id del valor seleccionado para cada uno de ellos. Los marcare en la respuesta con flechas `-->`

Para el ejemplo seleccionaremos el destino `Mundo` con la modalidad `Individual`

```json
{
    "opciones": [
        {
-->         "id": "245",
            "numero_parametro": 0,
            "nombre": "Destino",
            "tipo": "ambito_cobertura",
            "valores": [
                {
-->                 "id": "575",
                    "nombre": "Mundo",
                    "desde": "0",
                    "hasta": "0"
                },
                {
                    "id": "573",
                    "nombre": "Europa",
                    "desde": "0",
                    "hasta": "0"
                }
            ]
        },
        {
-->         "id": "246",
            "numero_parametro": 1,
            "nombre": "Modalidad",
            "tipo": "modalidad",
            "valores": [
                {
                    "id": "8428",
                    "nombre": "Familiar",
                    "desde": "0",
                    "hasta": "0"
                },
                {
-->                 "id": "571",
                    "nombre": "Individual",
                    "desde": "0",
                    "hasta": "0"
                }
            ]
        }
    ],
    "validaciones": {
        "viaje": {
            "fecha_modificacion": {
                "campo_referencia": "fecha_inicio",
                "valor": "-1",
                "requerido": null
            },
            "fecha_anulacion": {
                "campo_referencia": "fecha_inicio",
                "valor": "-1",
                "requerido": null
            },
            "fecha_inicio": {
                "requerido": null,
                "campo_referencia": "fecha_actual",
                "maximo": null,
                "minimo": null,
                "valor": "1"
            }
        }
    },
    "tipo_tarifa": "MODALIDAD_DESTINO"
}
```

**Obtener el precio de una póliza**

Con toda la información anterior ya estamos preparados para obtener el precio de nuestra poliza `13700` con destino `Mundo` y modalidad `Familiar`.

Realizamos la llamada:

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/es/polizas/13700/precios?parametros[0][id_parametro]=245&parametros[0][id_valor]=575&parametros[1][id_parametro]=246&parametros[1][id_valor]=571
```
<br />
  
**Solicitud CURL**

```
curl -X GET \
  'https://api.intermundial.com/v3/es/polizas/13700/precios?parametros[id_parametro]=245&parametros[id_valor]=575&parametros[id_parametro]=246&parametros[id_valor]=571'
  -H 'cache-control: no-cache'
```
<br />

**Respuesta**

En la respuesta se nos devuelve el `precio_base` del seguro y un flag que siempre tendremos que tener en cuenta `is_tarifa_por_asegurado`. Nos indica si debemos multiplicar los importes por el numero de pasajeros o no. Ademas recibiremos un arreglo llamado `coberturas_ampliables`. Este arreglo puede venir vacio, hay dos principales motivos por lo que esto ocurre:

* No se pueden ampliar las coberturas de la póliza.
* La configuración seleccionada no nos permite ampliar las coberturas.

En este caso concreto, el arreglo de `coberturas_ampliadas` viene con varias opciones.

Por lo tanto, si quisieramos saber el precio sin ampliaciones para un asegurado sería `312.74`, para dos `312.74 x 2 = 625.48` y asi consecutivamente.

Si observamos con detenimiento la estructura de una ampliación veremos varios campos importantes. El `id_cruce` es un identificador unico que identifica la ampliación para la configuración seleccionada de la póliza, es decir, destino `Mundo` y modalidad `Familiar`.

También podemos ver el `tipo`. Este valor no es relevante en la mayoría de los casos pero se solicita en la llamada de contratar un seguro.

Veremos que cada objeto de ampliación nos devuelve un arreglo de los posibles valores que pueden elegirse. Cada posible valor tiene varios campos a tener en cuenta:
  
* El `id` es un identificador único del valor. En la llamada de contratación se requiere.
* El flag `is_incluida` es un flag indica el valor que viene seleccionado por defecto en la cobertura en cuestión. El valor que tenga este flag a `true` indica que el valor de la cobertura en cuestión viene incluido en el `precio_base`.
* La `prima` es la cantidad de dinero que tenemos que sumar al precio base.
* El `limite` es la cantidad de dinero máximo que asegura la cobertura en cuestión para el valor seleccionado.

Vamos a ampliar la cobertura de `Anulación` y la cobertura de `Accidentes 24h`. Marcaré con `-->` aquellos valores que seleccionemos.

```json
{
    "precio_base": 312.74,
    "is_tarifa_por_asegurado": true,
    "coberturas_ampliables": [
        {
            "id": "10257",
            "id_cruce": "200000086237",
            "nombre": "Equipajes",
            "descripcion": "Si el valor de los objetos que llevas en tu equipaje supera el límite que lleva por defecto el seguro, amplía esta cobertura a tu gusto hasta conseguir la máxima protección. Recuerda que el material de fotografía, imagen y sonido, queda cubierto hasta el 50% del límite que selecciones.",
            "tipo": "incluida-lista",
            "valores": [
                {
                    "id": "6427",
                    "is_incluida": true,
                    "prima": 0,
                    "limite": 2100
                },
                {
                    "id": "6428",
                    "is_incluida": false,
                    "prima": 24.22,
                    "limite": 3100
                },
                {
                    "id": "6429",
                    "is_incluida": false,
                    "prima": 40.41,
                    "limite": 4100
                },
                {
                    "id": "6430",
                    "is_incluida": false,
                    "prima": 53.88,
                    "limite": 5100
                }
            ]
        },
        {
            "id": "8034",
-->         "id_cruce": "200000086205",
            "nombre": "Anulación",
            "descripcion": "Esta ampliación es especialmente recomendable si el coste total de la reserva de tu viaje supera el límite que lleva por defecto el seguro. Con ello te garantizas poder recuperar los gastos de cancelación de los billetes de transporte, las reservas hoteleras y otras actividades que hayas contratado. Ahora no hay excusa para no tener los gastos de tu viaje asegurados al 100%.",
-->         "tipo": "incluida-lista",
            "valores": [
                {
                    "id": "6447",
                    "is_incluida": true,
                    "prima": 0,
                    "limite": 5000
                },
                {
                    "id": "6448",
                    "is_incluida": false,
                    "prima": 33.67,
                    "limite": 6000
                },
                {
-->                 "id": "6449",
                    "is_incluida": false,
-->                 "prima": 55.2,
-->                 "limite": 7000
                },
                {
                    "id": "6450",
                    "is_incluida": false,
                    "prima": 83.48,
                    "limite": 8000
                },
                {
                    "id": "6451",
                    "is_incluida": false,
                    "prima": 106.74,
                    "limite": 9000
                },
                {
                    "id": "6452",
                    "is_incluida": false,
                    "prima": 120.27,
                    "limite": 10000
                },
                {
                    "id": "6453",
                    "is_incluida": false,
                    "prima": 137.51,
                    "limite": 11000
                }
            ]
        },
        {
            "id": "9156",
-->         "id_cruce": "200000086211",
            "nombre": "Accidentes 24h",
            "descripcion": "Protege el futuro de tu pareja, familia o legado. La cobertura de accidentes es la forma más segura de garantizar una fuente de ingresos y mantener vuestro nivel de vida en caso de que te ocurriera algo durante tu viaje. El límite de edad para contratar la ampliación de Accidentes es de 70 años.",
-->         "tipo": "incluida-lista",
            "valores": [
                {
                    "id": "6435",
                    "is_incluida": true,
                    "prima": 0,
                    "limite": 30000
                },
                {
-->                 "id": "6436",
                    "is_incluida": false,
-->                 "prima": 15.65,
-->                 "limite": 60000
                },
                {
                    "id": "6437",
                    "is_incluida": false,
                    "prima": 26.09,
                    "limite": 80000
                },
                {
                    "id": "6438",
                    "is_incluida": false,
                    "prima": 44.83,
                    "limite": 130000
                }
            ]
        }
    ]
}
``` 

La formula final quedaría así:

* `precio_base` = `312.74 x 2 = 625.48`
* `precio_ampliaciones` = `( 55.2 + 15.65 ) x 2`
* `precio_total` = `precio_base + precio_ampliaciones`

Con esto ya hemos tarificado el seguro y hemos ampliado algunas de sus coberturas.
Los datos que se han  seleccionado con la `-->` se requeriran en la llamada de contratar un seguro.