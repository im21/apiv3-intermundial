# Autorizacion de llamadas a la api con token

Procedimiento a seguir par autorizar de manera correcta las llamadas a la API.

En la llamada de login se nos devuelve el `token_sesion`. Este token es de tipo `bearer` y se pasa en la cabecera `authorization` de la siguiente manera:
 
```
curl -X GET \
  https://api.intermundial.com/v3/es/polizas \
  -H 'authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MDU5Mjc4NzAsImVuY3J5cHRlZF9zZXNzaW9uX2lkIjoiNTkwNTRhMmNiNmY1MTE5ZGQ3ODQxOWJmYWQ2MDhhMzRlN2VlNjgxNmYyMmM2M2I5OGE5ODE0Mzg2ZGU4NDY0ZSJ9.FQrncpR6lHjQwERAsY876E_U01nkKhcWhSTpnorwOGE' \
  -H 'cache-control: no-cache'
```