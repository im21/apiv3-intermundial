# Listado de paises

Permite recuperar el listado de paises.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/{locale}/paises.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
<br />

**Cuerpo de la solicitud**

El cuerpo de la solictud, al ser una consulta no es necesario

```json
```

<br />

**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/paises \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

## Respuesta con éxito

**Cuerpo de la respuesta**

```json
[
    {
        "id": 250,
        "nombre": "Afganistán",
        "continente": "Southern Asia",
        "capital": "Kabul",
        "codigo_pais": "AFG"
    },
    {
        "id": 254,
        "nombre": "Albania",
        "continente": "Southern Europe",
        "capital": "Tirana",
        "codigo_pais": "ALB"
    },
    {
        "id": 307,
        "nombre": "Alemania",
        "continente": "Western Europe",
        "capital": "Berlin",
        "codigo_pais": "DEU"
    },
    {
        "id": 255,
        "nombre": "Andorra",
        "continente": "Southern Europe",
        "capital": "Andorra la Vella",
        "codigo_pais": "AND"
    },
    {
        "id": 251,
        "nombre": "Angola",
        "continente": "Middle Africa",
        "capital": "Luanda",
        "codigo_pais": "AGO"
    },
    {
        "id": 252,
        "nombre": "Anguila",
        "continente": "Caribbean",
        "capital": "The Valley",
        "codigo_pais": "AIA"
    },
    {
        "id": 262,
        "nombre": "Antigua y Barbuda",
        "continente": "Caribbean",
        "capital": "Saint John's",
        "codigo_pais": "ATG"
    },
    {
        "id": 440,
        "nombre": "Arabia Saudita",
        "continente": "Western Asia",
        "capital": "Riyadh",
        "codigo_pais": "SAU"
    },
    {
        "id": 312,
        "nombre": "Argelia",
        "continente": "Northern Africa",
        "capital": "Algiers",
        "codigo_pais": "DZA"
    },
    {
        "id": 257,
        "nombre": "Argentina",
        "continente": "South America",
        "capital": "Buenos Aires",
        "codigo_pais": "ARG"
    },
    {
        "id": 258,
        "nombre": "Armenia",
        "continente": "Western Asia",
        "capital": "Yerevan",
        "codigo_pais": "ARM"
    },
    {
        "id": 249,
        "nombre": "Aruba",
        "continente": "Caribbean",
        "capital": "Oranjestad",
        "codigo_pais": "ABW"
    },
    {
        "id": 263,
        "nombre": "Australia",
        "continente": "Australia and New Zealand",
        "capital": "Canberra",
        "codigo_pais": "AUS"
    },
    {
        "id": 264,
        "nombre": "Austria",
        "continente": "Western Europe",
        "capital": "Vienna",
        "codigo_pais": "AUT"
    },
    {
        "id": 265,
        "nombre": "Azerbaiyán",
        "continente": "Western Asia",
        "capital": "Baku",
        "codigo_pais": "AZE"
    },
    {
        "id": 273,
        "nombre": "Bahamas",
        "continente": "Caribbean",
        "capital": "Nassau",
        "codigo_pais": "BHS"
    },
    {
        "id": 270,
        "nombre": "Bangladesh",
        "continente": "Southern Asia",
        "capital": "Dhaka",
        "codigo_pais": "BGD"
    },
    {
        "id": 281,
        "nombre": "Barbados",
        "continente": "Caribbean",
        "capital": "Bridgetown",
        "codigo_pais": "BRB"
    },
    {
        "id": 272,
        "nombre": "Baréin",
        "continente": "Western Asia",
        "capital": "Manama",
        "codigo_pais": "BHR"
    },
    {
        "id": 267,
        "nombre": "Bélgica",
        "continente": "Western Europe",
        "capital": "Brussels",
        "codigo_pais": "BEL"
    },
    {
        "id": 277,
        "nombre": "Belice",
        "continente": "Central America",
        "capital": "Belmopan",
        "codigo_pais": "BLZ"
    },
    {
        "id": 268,
        "nombre": "Benín",
        "continente": "Western Africa",
        "capital": "Porto-Novo",
        "codigo_pais": "BEN"
    },
    {
        "id": 278,
        "nombre": "Bermudas",
        "continente": "Northern America",
        "capital": "Hamilton",
        "codigo_pais": "BMU"
    },
    {
        "id": 276,
        "nombre": "Bielorrusia",
        "continente": "Eastern Europe",
        "capital": "Minsk",
        "codigo_pais": "BLR"
    },
    {
        "id": 396,
        "nombre": "Birmania",
        "continente": "South-Eastern Asia",
        "capital": "Naypyidaw",
        "codigo_pais": "MMR"
    },
    {
        "id": 279,
        "nombre": "Bolivia",
        "continente": "South America",
        "capital": "Sucre",
        "codigo_pais": "BOL"
    },
    {
        "id": 274,
        "nombre": "Bosnia y Herzegovina",
        "continente": "Southern Europe",
        "capital": "Sarajevo",
        "codigo_pais": "BIH"
    },
    {
        "id": 285,
        "nombre": "Botsuana",
        "continente": "Southern Africa",
        "capital": "Gaborone",
        "codigo_pais": "BWA"
    },
    {
        "id": 280,
        "nombre": "Brasil",
        "continente": "South America",
        "capital": "Brasília",
        "codigo_pais": "BRA"
    },
    {
        "id": 282,
        "nombre": "Brunéi",
        "continente": "South-Eastern Asia",
        "capital": "Bandar Seri Begawan",
        "codigo_pais": "BRN"
    },
    {
        "id": 271,
        "nombre": "Bulgaria",
        "continente": "Eastern Europe",
        "capital": "Sofia",
        "codigo_pais": "BGR"
    },
    {
        "id": 269,
        "nombre": "Burkina Faso",
        "continente": "Western Africa",
        "capital": "Ouagadougou",
        "codigo_pais": "BFA"
    },
    {
        "id": 266,
        "nombre": "Burundi",
        "continente": "Eastern Africa",
        "capital": "Bujumbura",
        "codigo_pais": "BDI"
    },
    {
        "id": 283,
        "nombre": "Bután",
        "continente": "Southern Asia",
        "capital": "Thimphu",
        "codigo_pais": "BTN"
    },
    {
        "id": 299,
        "nombre": "Cabo Verde",
        "continente": "Western Africa",
        "capital": "Praia",
        "codigo_pais": "CPV"
    },
    {
        "id": 367,
        "nombre": "Camboya",
        "continente": "South-Eastern Asia",
        "capital": "Phnom Penh",
        "codigo_pais": "KHM"
    },
    {
        "id": 293,
        "nombre": "Camerún",
        "continente": "Middle Africa",
        "capital": "Yaoundé",
        "codigo_pais": "CMR"
    },
    {
        "id": 287,
        "nombre": "Canadá",
        "continente": "Northern America",
        "capital": "Ottawa",
        "codigo_pais": "CAN"
    },
    {
        "id": 435,
        "nombre": "Catar",
        "continente": "Western Asia",
        "capital": "Doha",
        "codigo_pais": "QAT"
    },
    {
        "id": 464,
        "nombre": "Chad",
        "continente": "Middle Africa",
        "capital": "N'Djamena",
        "codigo_pais": "TCD"
    },
    {
        "id": 290,
        "nombre": "Chile",
        "continente": "South America",
        "capital": "Santiago",
        "codigo_pais": "CHL"
    },
    {
        "id": 291,
        "nombre": "China",
        "continente": "Eastern Asia",
        "capital": "Beijing",
        "codigo_pais": "CHN"
    },
    {
        "id": 305,
        "nombre": "Chipre",
        "continente": "Eastern Europe",
        "capital": "Nicosia",
        "codigo_pais": "CYP"
    },
    {
        "id": 484,
        "nombre": "Ciudad del Vaticano",
        "continente": "Southern Europe",
        "capital": "Vatican City",
        "codigo_pais": "VAT"
    },
    {
        "id": 297,
        "nombre": "Colombia",
        "continente": "South America",
        "capital": "Bogotá",
        "codigo_pais": "COL"
    },
    {
        "id": 298,
        "nombre": "Comoras",
        "continente": "Eastern Africa",
        "capital": "Moroni",
        "codigo_pais": "COM"
    },
    {
        "id": 430,
        "nombre": "Corea del Norte",
        "continente": "Eastern Asia",
        "capital": "Pyongyang",
        "codigo_pais": "PRK"
    },
    {
        "id": 370,
        "nombre": "Corea del Sur",
        "continente": "Eastern Asia",
        "capital": "Seoul",
        "codigo_pais": "KOR"
    },
    {
        "id": 292,
        "nombre": "Costa de Marfil",
        "continente": "Western Africa",
        "capital": "Yamoussoukro",
        "codigo_pais": "CIV"
    },
    {
        "id": 300,
        "nombre": "Costa Rica",
        "continente": "Central America",
        "capital": "San José",
        "codigo_pais": "CRI"
    },
    {
        "id": 347,
        "nombre": "Croacia",
        "continente": "Southern Europe",
        "capital": "Zagreb",
        "codigo_pais": "HRV"
    },
    {
        "id": 301,
        "nombre": "Cuba",
        "continente": "Caribbean",
        "capital": "Havana",
        "codigo_pais": "CUB"
    },
    {
        "id": 302,
        "nombre": "Curazao",
        "continente": "Caribbean",
        "capital": "Willemstad",
        "codigo_pais": "CUW"
    },
    {
        "id": 310,
        "nombre": "Dinamarca",
        "continente": "Northern Europe",
        "capital": "Copenhagen",
        "codigo_pais": "DNK"
    },
    {
        "id": 309,
        "nombre": "Dominica",
        "continente": "Caribbean",
        "capital": "Roseau",
        "codigo_pais": "DMA"
    },
    {
        "id": 313,
        "nombre": "Ecuador",
        "continente": "South America",
        "capital": "Quito",
        "codigo_pais": "ECU"
    },
    {
        "id": 314,
        "nombre": "Egipto",
        "continente": "Northern Africa",
        "capital": "Cairo",
        "codigo_pais": "EGY"
    },
    {
        "id": 448,
        "nombre": "El Salvador",
        "continente": "Central America",
        "capital": "San Salvador",
        "codigo_pais": "SLV"
    },
    {
        "id": 256,
        "nombre": "Emiratos Árabes Unidos",
        "continente": "Western Asia",
        "capital": "Abu Dhabi",
        "codigo_pais": "ARE"
    },
    {
        "id": 315,
        "nombre": "Eritrea",
        "continente": "Eastern Africa",
        "capital": "Asmara",
        "codigo_pais": "ERI"
    },
    {
        "id": 498,
        "nombre": "Escocia",
        "continente": "Northern Europe",
        "capital": "Edimburgo",
        "codigo_pais": "001"
    },
    {
        "id": 456,
        "nombre": "Eslovaquia",
        "continente": "Central Europe",
        "capital": "Bratislava",
        "codigo_pais": "SVK"
    },
    {
        "id": 457,
        "nombre": "Eslovenia",
        "continente": "Southern Europe",
        "capital": "Ljubljana",
        "codigo_pais": "SVN"
    },
    {
        "id": 317,
        "nombre": "España",
        "continente": "Southern Europe",
        "capital": "Madrid",
        "codigo_pais": "ESP"
    },
    {
        "id": 482,
        "nombre": "Estados Unidos",
        "continente": "Northern America",
        "capital": "Washington D.C.",
        "codigo_pais": "USA"
    },
    {
        "id": 318,
        "nombre": "Estonia",
        "continente": "Northern Europe",
        "capital": "Tallinn",
        "codigo_pais": "EST"
    },
    {
        "id": 319,
        "nombre": "Etiopía",
        "continente": "Eastern Africa",
        "capital": "Addis Ababa",
        "codigo_pais": "ETH"
    },
    {
        "id": 425,
        "nombre": "Filipinas",
        "continente": "South-Eastern Asia",
        "capital": "Manila",
        "codigo_pais": "PHL"
    },
    {
        "id": 320,
        "nombre": "Finlandia",
        "continente": "Northern Europe",
        "capital": "Helsinki",
        "codigo_pais": "FIN"
    },
    {
        "id": 321,
        "nombre": "Fiyi",
        "continente": "Melanesia",
        "capital": "Suva",
        "codigo_pais": "FJI"
    },
    {
        "id": 323,
        "nombre": "Francia",
        "continente": "Western Europe",
        "capital": "Paris",
        "codigo_pais": "FRA"
    },
    {
        "id": 326,
        "nombre": "Gabón",
        "continente": "Middle Africa",
        "capital": "Libreville",
        "codigo_pais": "GAB"
    },
    {
        "id": 499,
        "nombre": "Gales",
        "continente": "Northern Europe",
        "capital": "Cardiff",
        "codigo_pais": "002"
    },
    {
        "id": 334,
        "nombre": "Gambia",
        "continente": "Western Africa",
        "capital": "Banjul",
        "codigo_pais": "GMB"
    },
    {
        "id": 328,
        "nombre": "Georgia",
        "continente": "Western Asia",
        "capital": "Tbilisi",
        "codigo_pais": "GEO"
    },
    {
        "id": 330,
        "nombre": "Ghana",
        "continente": "Western Africa",
        "capital": "Accra",
        "codigo_pais": "GHA"
    },
    {
        "id": 331,
        "nombre": "Gibraltar",
        "continente": "Southern Europe",
        "capital": "Gibraltar",
        "codigo_pais": "GIB"
    },
    {
        "id": 338,
        "nombre": "Granada",
        "continente": "Caribbean",
        "capital": "St. George's",
        "codigo_pais": "GRD"
    },
    {
        "id": 337,
        "nombre": "Grecia",
        "continente": "Southern Europe",
        "capital": "Athens",
        "codigo_pais": "GRC"
    },
    {
        "id": 339,
        "nombre": "Groenlandia",
        "continente": "Northern America",
        "capital": "Nuuk",
        "codigo_pais": "GRL"
    },
    {
        "id": 333,
        "nombre": "Guadalupe",
        "continente": "Caribbean",
        "capital": "Basse-Terre",
        "codigo_pais": "GLP"
    },
    {
        "id": 342,
        "nombre": "Guam",
        "continente": "Micronesia",
        "capital": "Hagåtña",
        "codigo_pais": "GUM"
    },
    {
        "id": 340,
        "nombre": "Guatemala",
        "continente": "Central America",
        "capital": "Guatemala City",
        "codigo_pais": "GTM"
    },
    {
        "id": 341,
        "nombre": "Guayana Francesa",
        "continente": "South America",
        "capital": "Cayenne",
        "codigo_pais": "GUF"
    },
    {
        "id": 329,
        "nombre": "Guernsey",
        "continente": "Northern Europe",
        "capital": "St. Peter Port",
        "codigo_pais": "GGY"
    },
    {
        "id": 332,
        "nombre": "Guinea",
        "continente": "Western Africa",
        "capital": "Conakry",
        "codigo_pais": "GIN"
    },
    {
        "id": 335,
        "nombre": "Guinea Bisáu",
        "continente": "Western Africa",
        "capital": "Bissau",
        "codigo_pais": "GNB"
    },
    {
        "id": 336,
        "nombre": "Guinea Ecuatorial",
        "continente": "Middle Africa",
        "capital": "Malabo",
        "codigo_pais": "GNQ"
    },
    {
        "id": 343,
        "nombre": "Guyana",
        "continente": "South America",
        "capital": "Georgetown",
        "codigo_pais": "GUY"
    },
    {
        "id": 348,
        "nombre": "Haití",
        "continente": "Caribbean",
        "capital": "Port-au-Prince",
        "codigo_pais": "HTI"
    },
    {
        "id": 500,
        "nombre": "Holanda",
        "continente": "Western Europe",
        "capital": "Ámsterdam",
        "codigo_pais": "NLD"
    },
    {
        "id": 346,
        "nombre": "Honduras",
        "continente": "Central America",
        "capital": "Tegucigalpa",
        "codigo_pais": "HND"
    },
    {
        "id": 344,
        "nombre": "Hong Kong",
        "continente": "Eastern Asia",
        "capital": "City of Victoria",
        "codigo_pais": "HKG"
    },
    {
        "id": 349,
        "nombre": "Hungría",
        "continente": "Eastern Europe",
        "capital": "Budapest",
        "codigo_pais": "HUN"
    },
    {
        "id": 352,
        "nombre": "India",
        "continente": "Southern Asia",
        "capital": "New Delhi",
        "codigo_pais": "IND"
    },
    {
        "id": 350,
        "nombre": "Indonesia",
        "continente": "South-Eastern Asia",
        "capital": "Jakarta",
        "codigo_pais": "IDN"
    },
    {
        "id": 501,
        "nombre": "Inglaterra",
        "continente": "Northern Europe",
        "capital": "Londres",
        "codigo_pais": "004"
    },
    {
        "id": 356,
        "nombre": "Irak",
        "continente": "Western Asia",
        "capital": "Baghdad",
        "codigo_pais": "IRQ"
    },
    {
        "id": 355,
        "nombre": "Irán",
        "continente": "Southern Asia",
        "capital": "Tehran",
        "codigo_pais": "IRN"
    },
    {
        "id": 354,
        "nombre": "Irlanda",
        "continente": "Northern Europe",
        "capital": "Dublin",
        "codigo_pais": "IRL"
    },
    {
        "id": 502,
        "nombre": "Irlanda del norte",
        "continente": "Northern Europe",
        "capital": "Belfast",
        "codigo_pais": "003"
    },
    {
        "id": 351,
        "nombre": "Isla de Man",
        "continente": "Northern Europe",
        "capital": "Douglas",
        "codigo_pais": "IMN"
    },
    {
        "id": 303,
        "nombre": "Isla de Navidad",
        "continente": "Australia and New Zealand",
        "capital": "Flying Fish Cove",
        "codigo_pais": "CXR"
    },
    {
        "id": 411,
        "nombre": "Isla Norfolk",
        "continente": "Australia and New Zealand",
        "capital": "Kingston",
        "codigo_pais": "NFK"
    },
    {
        "id": 357,
        "nombre": "Islandia",
        "continente": "Northern Europe",
        "capital": "Reykjavik",
        "codigo_pais": "ISL"
    },
    {
        "id": 304,
        "nombre": "Islas Caimán",
        "continente": "Caribbean",
        "capital": "George Town",
        "codigo_pais": "CYM"
    },
    {
        "id": 288,
        "nombre": "Islas Cocos o Islas Keeling",
        "continente": "Australia and New Zealand",
        "capital": "West Island",
        "codigo_pais": "CCK"
    },
    {
        "id": 296,
        "nombre": "Islas Cook",
        "continente": "Polynesia",
        "capital": "Avarua",
        "codigo_pais": "COK"
    },
    {
        "id": 324,
        "nombre": "Islas Feroe",
        "continente": "Northern Europe",
        "capital": "Tórshavn",
        "codigo_pais": "FRO"
    },
    {
        "id": 444,
        "nombre": "Islas Georgias del Sur y Sandwich del Sur",
        "continente": "South America",
        "capital": "King Edward Point",
        "codigo_pais": "SGS"
    },
    {
        "id": 445,
        "nombre": "Islas Svalbard y Jan Mayen",
        "continente": "Northern Europe",
        "capital": "Longyearbyen",
        "codigo_pais": "SJM"
    },
    {
        "id": 463,
        "nombre": "Islas Turcas y Caicos",
        "continente": "Caribbean",
        "capital": "Cockburn Town",
        "codigo_pais": "TCA"
    },
    {
        "id": 487,
        "nombre": "Islas Vírgenes Británicas",
        "continente": "Caribbean",
        "capital": "Road Town",
        "codigo_pais": "VGB"
    },
    {
        "id": 488,
        "nombre": "Islas Vírgenes de los Estados Unidos",
        "continente": "Caribbean",
        "capital": "Charlotte Amalie",
        "codigo_pais": "VIR"
    },
    {
        "id": 358,
        "nombre": "Israel",
        "continente": "Western Asia",
        "capital": "Jerusalem",
        "codigo_pais": "ISR"
    },
    {
        "id": 359,
        "nombre": "Italia",
        "continente": "Southern Europe",
        "capital": "Rome",
        "codigo_pais": "ITA"
    },
    {
        "id": 360,
        "nombre": "Jamaica",
        "continente": "Caribbean",
        "capital": "Kingston",
        "codigo_pais": "JAM"
    },
    {
        "id": 363,
        "nombre": "Japón",
        "continente": "Eastern Asia",
        "capital": "Tokyo",
        "codigo_pais": "JPN"
    },
    {
        "id": 361,
        "nombre": "Jersey",
        "continente": "Northern Europe",
        "capital": "Saint Helier",
        "codigo_pais": "JEY"
    },
    {
        "id": 362,
        "nombre": "Jordania",
        "continente": "Western Asia",
        "capital": "Amman",
        "codigo_pais": "JOR"
    },
    {
        "id": 364,
        "nombre": "Kazajistán",
        "continente": "Central Asia",
        "capital": "Astana",
        "codigo_pais": "KAZ"
    },
    {
        "id": 365,
        "nombre": "Kenia",
        "continente": "Eastern Africa",
        "capital": "Nairobi",
        "codigo_pais": "KEN"
    },
    {
        "id": 366,
        "nombre": "Kirguistán",
        "continente": "Central Asia",
        "capital": "Bishkek",
        "codigo_pais": "KGZ"
    },
    {
        "id": 368,
        "nombre": "Kiribati",
        "continente": "Micronesia",
        "capital": "South Tarawa",
        "codigo_pais": "KIR"
    },
    {
        "id": 371,
        "nombre": "Kosovo",
        "continente": "Eastern Europe",
        "capital": "Pristina",
        "codigo_pais": "005"
    },
    {
        "id": 372,
        "nombre": "Kuwait",
        "continente": "Western Asia",
        "capital": "Kuwait City",
        "codigo_pais": "KWT"
    },
    {
        "id": 373,
        "nombre": "Laos",
        "continente": "South-Eastern Asia",
        "capital": "Vientiane",
        "codigo_pais": "LAO"
    },
    {
        "id": 380,
        "nombre": "Lesoto",
        "continente": "Southern Africa",
        "capital": "Maseru",
        "codigo_pais": "LSO"
    },
    {
        "id": 383,
        "nombre": "Letonia",
        "continente": "Northern Europe",
        "capital": "Riga",
        "codigo_pais": "LVA"
    },
    {
        "id": 374,
        "nombre": "Líbano",
        "continente": "Western Asia",
        "capital": "Beirut",
        "codigo_pais": "LBN"
    },
    {
        "id": 375,
        "nombre": "Liberia",
        "continente": "Western Africa",
        "capital": "Monrovia",
        "codigo_pais": "LBR"
    },
    {
        "id": 376,
        "nombre": "Libia",
        "continente": "Northern Africa",
        "capital": "Tripoli",
        "codigo_pais": "LBY"
    },
    {
        "id": 378,
        "nombre": "Liechtenstein",
        "continente": "Western Europe",
        "capital": "Vaduz",
        "codigo_pais": "LIE"
    },
    {
        "id": 381,
        "nombre": "Lituania",
        "continente": "Northern Europe",
        "capital": "Vilnius",
        "codigo_pais": "LTU"
    },
    {
        "id": 382,
        "nombre": "Luxemburgo",
        "continente": "Western Europe",
        "capital": "Luxembourg",
        "codigo_pais": "LUX"
    },
    {
        "id": 393,
        "nombre": "Macedonia",
        "continente": "Southern Europe",
        "capital": "Skopje",
        "codigo_pais": "MKD"
    },
    {
        "id": 389,
        "nombre": "Madagascar",
        "continente": "Eastern Africa",
        "capital": "Antananarivo",
        "codigo_pais": "MDG"
    },
    {
        "id": 406,
        "nombre": "Malasia",
        "continente": "South-Eastern Asia",
        "capital": "Kuala Lumpur",
        "codigo_pais": "MYS"
    },
    {
        "id": 405,
        "nombre": "Malaui",
        "continente": "Eastern Africa",
        "capital": "Lilongwe",
        "codigo_pais": "MWI"
    },
    {
        "id": 390,
        "nombre": "Maldivas",
        "continente": "Southern Asia",
        "capital": "Malé",
        "codigo_pais": "MDV"
    },
    {
        "id": 394,
        "nombre": "Malí",
        "continente": "Western Africa",
        "capital": "Bamako",
        "codigo_pais": "MLI"
    },
    {
        "id": 395,
        "nombre": "Malta",
        "continente": "Southern Europe",
        "capital": "Valletta",
        "codigo_pais": "MLT"
    },
    {
        "id": 322,
        "nombre": "Malvinas, Islas",
        "continente": "South America",
        "capital": "Stanley",
        "codigo_pais": "FLK"
    },
    {
        "id": 399,
        "nombre": "Marianas del Norte, Islas",
        "continente": "Micronesia",
        "capital": "Saipan",
        "codigo_pais": "MNP"
    },
    {
        "id": 386,
        "nombre": "Marruecos",
        "continente": "Northern Africa",
        "capital": "Rabat",
        "codigo_pais": "MAR"
    },
    {
        "id": 392,
        "nombre": "Marshall, Islas",
        "continente": "Micronesia",
        "capital": "Majuro",
        "codigo_pais": "MHL"
    },
    {
        "id": 403,
        "nombre": "Martinica",
        "continente": "Caribbean",
        "capital": "Fort-de-France",
        "codigo_pais": "MTQ"
    },
    {
        "id": 404,
        "nombre": "Mauricio",
        "continente": "Eastern Africa",
        "capital": "Port Louis",
        "codigo_pais": "MUS"
    },
    {
        "id": 401,
        "nombre": "Mauritania",
        "continente": "Western Africa",
        "capital": "Nouakchott",
        "codigo_pais": "MRT"
    },
    {
        "id": 407,
        "nombre": "Mayotte",
        "continente": "Eastern Africa",
        "capital": "Mamoudzou",
        "codigo_pais": "MYT"
    },
    {
        "id": 391,
        "nombre": "México",
        "continente": "Central America",
        "capital": "Mexico City",
        "codigo_pais": "MEX"
    },
    {
        "id": 325,
        "nombre": "Micronesia",
        "continente": "Micronesia",
        "capital": "Palikir",
        "codigo_pais": "FSM"
    },
    {
        "id": 388,
        "nombre": "Moldavia",
        "continente": "Eastern Europe",
        "capital": "Chișinău",
        "codigo_pais": "MDA"
    },
    {
        "id": 387,
        "nombre": "Mónaco",
        "continente": "Western Europe",
        "capital": "Monaco",
        "codigo_pais": "MCO"
    },
    {
        "id": 398,
        "nombre": "Mongolia",
        "continente": "Eastern Asia",
        "capital": "Ulan Bator",
        "codigo_pais": "MNG"
    },
    {
        "id": 397,
        "nombre": "Montenegro",
        "continente": "Southern Europe",
        "capital": "Podgorica",
        "codigo_pais": "MNE"
    },
    {
        "id": 402,
        "nombre": "Montserrat",
        "continente": "Caribbean",
        "capital": "Plymouth",
        "codigo_pais": "MSR"
    },
    {
        "id": 400,
        "nombre": "Mozambique",
        "continente": "Eastern Africa",
        "capital": "Maputo",
        "codigo_pais": "MOZ"
    },
    {
        "id": 408,
        "nombre": "Namibia",
        "continente": "Southern Africa",
        "capital": "Windhoek",
        "codigo_pais": "NAM"
    },
    {
        "id": 418,
        "nombre": "Nauru",
        "continente": "Micronesia",
        "capital": "Yaren",
        "codigo_pais": "NRU"
    },
    {
        "id": 417,
        "nombre": "Nepal",
        "continente": "Southern Asia",
        "capital": "Kathmandu",
        "codigo_pais": "NPL"
    },
    {
        "id": 413,
        "nombre": "Nicaragua",
        "continente": "Central America",
        "capital": "Managua",
        "codigo_pais": "NIC"
    },
    {
        "id": 410,
        "nombre": "Níger",
        "continente": "Western Africa",
        "capital": "Niamey",
        "codigo_pais": "NER"
    },
    {
        "id": 412,
        "nombre": "Nigeria",
        "continente": "Western Africa",
        "capital": "Abuja",
        "codigo_pais": "NGA"
    },
    {
        "id": 414,
        "nombre": "Niue",
        "continente": "Polynesia",
        "capital": "Alofi",
        "codigo_pais": "NIU"
    },
    {
        "id": 416,
        "nombre": "Noruega",
        "continente": "Northern Europe",
        "capital": "Oslo",
        "codigo_pais": "NOR"
    },
    {
        "id": 409,
        "nombre": "Nueva Caledonia",
        "continente": "Melanesia",
        "capital": "Nouméa",
        "codigo_pais": "NCL"
    },
    {
        "id": 419,
        "nombre": "Nueva Zelanda",
        "continente": "Australia and New Zealand",
        "capital": "Wellington",
        "codigo_pais": "NZL"
    },
    {
        "id": 420,
        "nombre": "Omán",
        "continente": "Western Asia",
        "capital": "Muscat",
        "codigo_pais": "OMN"
    },
    {
        "id": 415,
        "nombre": "Países Bajos",
        "continente": "Western Europe",
        "capital": "Amsterdam",
        "codigo_pais": "NLD"
    },
    {
        "id": 421,
        "nombre": "Pakistán",
        "continente": "Southern Asia",
        "capital": "Islamabad",
        "codigo_pais": "PAK"
    },
    {
        "id": 426,
        "nombre": "Palaos",
        "continente": "Micronesia",
        "capital": "Ngerulmud",
        "codigo_pais": "PLW"
    },
    {
        "id": 433,
        "nombre": "Palestina",
        "continente": "Western Asia",
        "capital": "Ramallah",
        "codigo_pais": "PSE"
    },
    {
        "id": 422,
        "nombre": "Panamá",
        "continente": "Central America",
        "capital": "Panama City",
        "codigo_pais": "PAN"
    },
    {
        "id": 427,
        "nombre": "Papúa Nueva Guinea",
        "continente": "Melanesia",
        "capital": "Port Moresby",
        "codigo_pais": "PNG"
    },
    {
        "id": 432,
        "nombre": "Paraguay",
        "continente": "South America",
        "capital": "Asunción",
        "codigo_pais": "PRY"
    },
    {
        "id": 424,
        "nombre": "Perú",
        "continente": "South America",
        "capital": "Lima",
        "codigo_pais": "PER"
    },
    {
        "id": 423,
        "nombre": "Pitcairn, Islas",
        "continente": "Polynesia",
        "capital": "Adamstown",
        "codigo_pais": "PCN"
    },
    {
        "id": 434,
        "nombre": "Polinesia Francesa",
        "continente": "Polynesia",
        "capital": "Papeetē",
        "codigo_pais": "PYF"
    },
    {
        "id": 428,
        "nombre": "Polonia",
        "continente": "Eastern Europe",
        "capital": "Warsaw",
        "codigo_pais": "POL"
    },
    {
        "id": 431,
        "nombre": "Portugal",
        "continente": "Southern Europe",
        "capital": "Lisbon",
        "codigo_pais": "PRT"
    },
    {
        "id": 429,
        "nombre": "Puerto Rico",
        "continente": "Caribbean",
        "capital": "San Juan",
        "codigo_pais": "PRI"
    },
    {
        "id": 327,
        "nombre": "Reino Unido",
        "continente": "Northern Europe",
        "capital": "London",
        "codigo_pais": "GBR"
    },
    {
        "id": 286,
        "nombre": "República Centroafricana",
        "continente": "Middle Africa",
        "capital": "Bangui",
        "codigo_pais": "CAF"
    },
    {
        "id": 306,
        "nombre": "República Checa",
        "continente": "Eastern Europe",
        "capital": "Prague",
        "codigo_pais": "CZE"
    },
    {
        "id": 308,
        "nombre": "República de Yibuti",
        "continente": "Eastern Africa",
        "capital": "Djibouti",
        "codigo_pais": "DJI"
    },
    {
        "id": 295,
        "nombre": "República del Congo",
        "continente": "Middle Africa",
        "capital": "Brazzaville",
        "codigo_pais": "COG"
    },
    {
        "id": 294,
        "nombre": "República Democrática del Congo",
        "continente": "Middle Africa",
        "capital": "Kinshasa",
        "codigo_pais": "COD"
    },
    {
        "id": 311,
        "nombre": "República Dominicana",
        "continente": "Caribbean",
        "capital": "Santo Domingo",
        "codigo_pais": "DOM"
    },
    {
        "id": 436,
        "nombre": "Reunión",
        "continente": "Eastern Africa",
        "capital": "Saint-Denis",
        "codigo_pais": "REU"
    },
    {
        "id": 439,
        "nombre": "Ruanda",
        "continente": "Eastern Africa",
        "capital": "Kigali",
        "codigo_pais": "RWA"
    },
    {
        "id": 437,
        "nombre": "Rumania",
        "continente": "Eastern Europe",
        "capital": "Bucharest",
        "codigo_pais": "ROU"
    },
    {
        "id": 503,
        "nombre": "Rusia ( zona asiática )",
        "continente": "Eastern Asia",
        "capital": "Moscow",
        "codigo_pais": "RUS"
    },
    {
        "id": 438,
        "nombre": "Rusia ( zona europea )",
        "continente": "Eastern Europe",
        "capital": "Moscow",
        "codigo_pais": "RUS"
    },
    {
        "id": 316,
        "nombre": "Sahara Occidental",
        "continente": "Northern Africa",
        "capital": "El Aaiún",
        "codigo_pais": "ESH"
    },
    {
        "id": 446,
        "nombre": "Salomón, Islas",
        "continente": "Melanesia",
        "capital": "Honiara",
        "codigo_pais": "SLB"
    },
    {
        "id": 492,
        "nombre": "Samoa",
        "continente": "Polynesia",
        "capital": "Apia",
        "codigo_pais": "WSM"
    },
    {
        "id": 259,
        "nombre": "Samoa Americana",
        "continente": "Polynesia",
        "capital": "Pago Pago",
        "codigo_pais": "ASM"
    },
    {
        "id": 275,
        "nombre": "San Bartolomé",
        "continente": "Caribbean",
        "capital": "Gustavia",
        "codigo_pais": "BLM"
    },
    {
        "id": 369,
        "nombre": "San Cristóbal y Nieves",
        "continente": "Caribbean",
        "capital": "Basseterre",
        "codigo_pais": "KNA"
    },
    {
        "id": 449,
        "nombre": "San Marino",
        "continente": "Southern Europe",
        "capital": "City of San Marino",
        "codigo_pais": "SMR"
    },
    {
        "id": 385,
        "nombre": "San Martin",
        "continente": "Caribbean",
        "capital": "Marigot",
        "codigo_pais": "SXM"
    },
    {
        "id": 451,
        "nombre": "San Pedro y Miquelón",
        "continente": "Northern America",
        "capital": "Saint-Pierre",
        "codigo_pais": "SPM"
    },
    {
        "id": 485,
        "nombre": "San Vicente y las Granadinas",
        "continente": "Caribbean",
        "capital": "Kingstown",
        "codigo_pais": "VCT"
    },
    {
        "id": 377,
        "nombre": "Santa Lucía",
        "continente": "Caribbean",
        "capital": "Castries",
        "codigo_pais": "LCA"
    },
    {
        "id": 454,
        "nombre": "Santo Tomé y Príncipe",
        "continente": "Middle Africa",
        "capital": "São Tomé",
        "codigo_pais": "STP"
    },
    {
        "id": 442,
        "nombre": "Senegal",
        "continente": "Western Africa",
        "capital": "Dakar",
        "codigo_pais": "SEN"
    },
    {
        "id": 452,
        "nombre": "Serbia",
        "continente": "Southern Europe",
        "capital": "Belgrade",
        "codigo_pais": "SRB"
    },
    {
        "id": 461,
        "nombre": "Seychelles",
        "continente": "Eastern Africa",
        "capital": "Victoria",
        "codigo_pais": "SYC"
    },
    {
        "id": 447,
        "nombre": "Sierra Leona",
        "continente": "Western Africa",
        "capital": "Freetown",
        "codigo_pais": "SLE"
    },
    {
        "id": 443,
        "nombre": "Singapur",
        "continente": "South-Eastern Asia",
        "capital": "Singapore",
        "codigo_pais": "SGP"
    },
    {
        "id": 460,
        "nombre": "Sint Maarten",
        "continente": "Caribbean",
        "capital": "Philipsburg",
        "codigo_pais": "SXM"
    },
    {
        "id": 462,
        "nombre": "Siria",
        "continente": "Western Asia",
        "capital": "Damascus",
        "codigo_pais": "SYR"
    },
    {
        "id": 450,
        "nombre": "Somalia",
        "continente": "Eastern Africa",
        "capital": "Mogadishu",
        "codigo_pais": "SOM"
    },
    {
        "id": 379,
        "nombre": "Sri Lanka",
        "continente": "Southern Asia",
        "capital": "Colombo",
        "codigo_pais": "LKA"
    },
    {
        "id": 459,
        "nombre": "Suazilandia",
        "continente": "Southern Africa",
        "capital": "Lobamba",
        "codigo_pais": "CHE"
    },
    {
        "id": 494,
        "nombre": "Sudáfrica",
        "continente": "Southern Africa",
        "capital": "Pretoria",
        "codigo_pais": "ZAF"
    },
    {
        "id": 441,
        "nombre": "Sudán",
        "continente": "Northern Africa",
        "capital": "Khartoum",
        "codigo_pais": "SDN"
    },
    {
        "id": 453,
        "nombre": "Sudán del Sur",
        "continente": "Middle Africa",
        "capital": "Juba",
        "codigo_pais": "SSD"
    },
    {
        "id": 458,
        "nombre": "Suecia",
        "continente": "Northern Europe",
        "capital": "Stockholm",
        "codigo_pais": "SWE"
    },
    {
        "id": 289,
        "nombre": "Suiza",
        "continente": "Western Europe",
        "capital": "Bern",
        "codigo_pais": "CHE"
    },
    {
        "id": 455,
        "nombre": "Surinam",
        "continente": "South America",
        "capital": "Paramaribo",
        "codigo_pais": "SUR"
    },
    {
        "id": 466,
        "nombre": "Tailandia",
        "continente": "South-Eastern Asia",
        "capital": "Bangkok",
        "codigo_pais": "THA"
    },
    {
        "id": 476,
        "nombre": "Taiwán",
        "continente": "Eastern Asia",
        "capital": "Taipei",
        "codigo_pais": "PSE"
    },
    {
        "id": 477,
        "nombre": "Tanzania",
        "continente": "Eastern Africa",
        "capital": "Dodoma",
        "codigo_pais": "TZA"
    },
    {
        "id": 467,
        "nombre": "Tayikistán",
        "continente": "Central Asia",
        "capital": "Dushanbe",
        "codigo_pais": "TJK"
    },
    {
        "id": 353,
        "nombre": "Territorio Británico del Océano Índico",
        "continente": "Eastern Africa",
        "capital": "Diego Garcia",
        "codigo_pais": "IOT"
    },
    {
        "id": 261,
        "nombre": "Tierras Australes y Antárticas Francesas",
        "continente": "",
        "capital": "Port-aux-Français",
        "codigo_pais": "ATF"
    },
    {
        "id": 470,
        "nombre": "Timor Oriental",
        "continente": "South-Eastern Asia",
        "capital": "Dili",
        "codigo_pais": "TLS"
    },
    {
        "id": 465,
        "nombre": "Togo",
        "continente": "Western Africa",
        "capital": "Lomé",
        "codigo_pais": "TGO"
    },
    {
        "id": 468,
        "nombre": "Tokelau",
        "continente": "Polynesia",
        "capital": "Fakaofo",
        "codigo_pais": "TKL"
    },
    {
        "id": 471,
        "nombre": "Tonga",
        "continente": "Polynesia",
        "capital": "Nuku'alofa",
        "codigo_pais": "TON"
    },
    {
        "id": 472,
        "nombre": "Trinidad y Tobago",
        "continente": "Caribbean",
        "capital": "Port of Spain",
        "codigo_pais": "TTO"
    },
    {
        "id": 473,
        "nombre": "Túnez",
        "continente": "Northern Africa",
        "capital": "Tunis",
        "codigo_pais": "TUN"
    },
    {
        "id": 469,
        "nombre": "Turkmenistán",
        "continente": "Central Asia",
        "capital": "Ashgabat",
        "codigo_pais": "TKM"
    },
    {
        "id": 474,
        "nombre": "Turquía",
        "continente": "Western Asia",
        "capital": "Ankara",
        "codigo_pais": "TUR"
    },
    {
        "id": 475,
        "nombre": "Tuvalu",
        "continente": "Polynesia",
        "capital": "Funafuti",
        "codigo_pais": "TUV"
    },
    {
        "id": 479,
        "nombre": "Ucrania",
        "continente": "Eastern Europe",
        "capital": "Kiev",
        "codigo_pais": "UKR"
    },
    {
        "id": 478,
        "nombre": "Uganda",
        "continente": "Eastern Africa",
        "capital": "Kampala",
        "codigo_pais": "UGA"
    },
    {
        "id": 481,
        "nombre": "Uruguay",
        "continente": "South America",
        "capital": "Montevideo",
        "codigo_pais": "URY"
    },
    {
        "id": 483,
        "nombre": "Uzbekistán",
        "continente": "Central Asia",
        "capital": "Tashkent",
        "codigo_pais": "UZB"
    },
    {
        "id": 490,
        "nombre": "Vanuatu",
        "continente": "Melanesia",
        "capital": "Port Vila",
        "codigo_pais": "VUT"
    },
    {
        "id": 486,
        "nombre": "Venezuela",
        "continente": "South America",
        "capital": "Caracas",
        "codigo_pais": "VEN"
    },
    {
        "id": 489,
        "nombre": "Vietnam",
        "continente": "South-Eastern Asia",
        "capital": "Hanoi",
        "codigo_pais": "VNM"
    },
    {
        "id": 491,
        "nombre": "Wallis y Futuna",
        "continente": "Polynesia",
        "capital": "Mata-Utu",
        "codigo_pais": "WLF"
    },
    {
        "id": 493,
        "nombre": "Yemen",
        "continente": "Western Asia",
        "capital": "Sana'a",
        "codigo_pais": "YEM"
    },
    {
        "id": 495,
        "nombre": "Zambia",
        "continente": "Eastern Africa",
        "capital": "Lusaka",
        "codigo_pais": "ZMB"
    },
    {
        "id": 496,
        "nombre": "Zimbabue",
        "continente": "Eastern Africa",
        "capital": "Harare",
        "codigo_pais": "ZWE"
    }
]
```
<br />

**Cabeceras de la respuesta**

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=5mjhiol487osk7jbm1lbmisa20; expires=Wed, 15-Nov-2017 13:06:19 GMT; Max-Age=3600; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />