# Detalle de un pais

Permite recuperar el detalle de un pais mediante su id.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/{locale}/paises/{id_pais}.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
id_pais | integer | Identificador unico del pais |
<br />

**Cuerpo de la solicitud**

El cuerpo de la solictud, al ser una consulta no es necesario

```json
```

<br />

**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/paises/250 \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

**Cuerpo de la respuesta**

```json
[
    {
        "id": 250,
        "nombre": "Afganistán",
        "continente": "Southern Asia",
        "capital": "Kabul",
        "codigo_pais": "AFG"
    }
}
```
<br />

**Cabeceras de la respuesta**

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=5mjhiol487osk7jbm1lbmisa20; expires=Wed, 15-Nov-2017 13:06:19 GMT; Max-Age=3600; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />