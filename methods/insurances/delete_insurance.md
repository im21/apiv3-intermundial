# Anular un seguro

Permite anular un seguro contratado siempre que la fecha de actual sea inferior a la fecha de inicio.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
DELETE https://api.intermundial.com/v3/{locale}/seguros/{id_seguro}.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
id_seguro | string | Identificador unico del seguro |
<br />

**Cuerpo de la solicitud**

El cuerpo de la solictud, al ser una solictud de anulación no es necesario.

```json
```

<br />

**Solicitud CURL**

```
curl -X DELETE \
  https://api-aleon.intermundial.com/v3/es/seguros/00000137000000000817 \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

El cuerpo de la respuesta estara vacio. Se retornara un código de estado 204.