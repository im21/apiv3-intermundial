# Obtiene el certificado de un seguro

Permite descargar el certificado de un seguro.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/{locale}/seguros/{id_seguro}/certificados.{?format}?pvp={?pvp}&language={?language}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
pvp | string | Si se pasa el valor pvp a `true`, los importes del certificado iran con el pvp incluido. Si se pasa pvp a `false` o no se pasa, los importes del certificado iran sin pvp incluido |
language | string | Código de lenguaje en formato `xx-ZZ`, donde `xx` es el código de país en formato [ISO 639-1](https://www.iso.org/iso-639-language-codes.html) y `ZZ` es el código de país en formato [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) [es-ES, en-GB, ...] |
<br />

**Solicitud CURL**

```
curl -X GET \
  https://api-aleon.intermundial.com/v3/es/seguros/00000170290000000002/certificados \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

Se devuelve el documento PDF del certificado en descarga.
<br />

**Cabeceras de la respuesta**

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Accept-Ranges bytes
Cache-Control public, max-age=2592000
Connection Keep-Alive
Content-Length 156171
Content-Type application/pdf
Date Thu, 31 Aug 2017 16:13:52 GMT
Expires Sat, 30 Sep 2017 16:13:52 GMT
Keep-Alive timeout=5, max=100
Last-Modified Thu, 31 Aug 2017 16:13:58 GMT
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=nlqlhhds2h65f0t0t8d4cvoln0; expires=Thu, 31-Aug-2017 18:13:52 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />