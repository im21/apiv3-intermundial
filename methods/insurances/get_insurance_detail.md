# Detalle de un seguro contratado

Obtiene la información detallada de un seguro contratado.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/{locale}/seguros/{id_seguro}.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
id_seguro | string | Identificador unico del seguro contratado |
<br />

**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/seguros/00000170290000000044 \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

Devuelve un objeto de seguro con toda la información. Referencias, precio, estado, asegurados, ampliaciones, opciones configuradas de la poliza, etc...

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **id**: Identificador del seguro. Permite trabajar en futuras llamadas relacionadas con el seguro.
* **poliza**: Información básica sobre la póliza.
* **localizador**: Numero de referencia que permite localizar el seguro en el sistema.
* **referencia_agencia**: Referencia externa a nuestro sistema. En el proceso de contratación se nos puede indicar dicha referencia para que la guardemos.
* **estado**: El estado del seguro. Hay dos estados posibles `alta` o `baja`.
* **is_tarifa_por_asegurado**: Indica si el importe del seguro 
* **asegurados**: Arreglo que devuelve un unico asegurado. Contiene la información básica del asegurado principal.
* **fecha_contratacion**: Fecha en la que se contrato el seguro.
* **ampliaciones**: Arreglo de las ampliaciones contrartadas por el seguro. Si no se contrataron ampliaciones se devuelve vacio.
* **parametros**: Arreglo de los parametros seleccionados para configurar el seguro. 
<br />

**Cuerpo de la respuesta**

```json
{
    "id": "00000170290000000044",
    "localizador": "266390",
    "referencia_agencia": null,
    "certificado_con_pvp_path": "https://api-aleon.intermundial.com/v3/app_dev.php/b2b/es/seguros/00000170290000000044/certificados?pvp=true",
    "certificado_sin_pvp_path": "https://api-aleon.intermundial.com/v3/app_dev.php/b2b/es/seguros/00000170290000000044/certificados?pvp=false",
    "fecha_inicio": "2017-08-21",
    "fecha_fin": "2017-08-25",
    "total_asegurados": "2",
    "pais_destino": "Italia",
    "pais_origen": "España",
    "poliza": {
        "id": "17029",
        "nombre_comercial": "PRUEBA PRIMA FIJA",
        "tipo_tarifa": "PRIMAFIJA"
    },
    "precio_base": "10",
    "precio_ampliaciones": null,
    "precio_total": "10",
    "is_tarifa_por_asegurado": true,
    "estado": "alta",
    "asegurados": [
        {
            "tratamiento": null,
            "nombre": null,
            "apellidos": "Gonzalo Test",
            "tipo_documento": null,
            "numero_documento": null,
            "direccion": null,
            "codigo_postal": null,
            "localidad": null,
            "provincia": null,
            "telefono": null,
            "email": "test@test.com",
            "fecha_nacimiento": null,
            "pais": null,
            "is_asegurado_principal": true
        },
        {
            "tratamiento": null,
            "nombre": null,
            "apellidos": "Pablo Test",
            "tipo_documento": null,
            "numero_documento": null,
            "fecha_nacimiento": null,
            "email": "test@test.com",
            "pais": null,
            "is_asegurado_principal": false
        }
    ],
    "fecha_contratacion": "2017-08-21",
    "ampliaciones": [],
    "parametros": [
        {
            "id": "912",
            "numero_parametro": 0,
            "nombre": "Importe fijo",
            "tipo": "prima_fija",
            "valores": [
                {
                    "id": "2429",
                    "nombre": "5,00 €"
                }
            ]
        }
    ]
}
```
<br />

**Cabeceras de la respuesta**

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />