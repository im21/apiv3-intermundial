# Listado de polizas contratables

Obtiene el listado de seguros del usuario.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/{locale}/seguros.{?format}?estado={?estado}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
estado | string | Filtra el listado de seguros para mostrar solo los seguros que tengan estado `alta` o `baja` |
<br />

**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/seguros \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

Devuelve un arreglo de seguros, con información parcial de la poliza, asegurado principal y referencias de la reserva.

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **id**: Identificador del seguro. Permite trabajar en futuras llamadas relacionadas con el seguro.
* **nombre_comercial**: Nombre comercial de la póliza.
* **localizador**: Numero de referencia que permite localizar el seguro en el sistema.
* **referencia_agencia**: Referencia externa a nuestro sistema. En el proceso de contratación se nos puede indicar dicha referencia para que la guardemos.
* **estado**: El estado del seguro. Hay dos estados posibles `alta` o `baja`.
* **asegurados**: Arreglo que devuelve un unico asegurado. Contiene la información básica del asegurado principal.
* **fecha_contratacion**: Fecha en la que se contrato el seguro.
<br />

**Cuerpo de la respuesta**

```json
[
    {
        "id": "00000170290000000044",
        "nombre_comercial": "PRUEBA PRIMA FIJA",
        "localizador": "266390",
        "referencia_agencia": null,
        "total_asegurados": "2",
        "estado": "alta",
        "asegurados": [
            {
                "numero_documento": null,
                "nombre": "Gonzalo Test",
                "apellidos": null
            }
        ],
        "fecha_contratacion": "21/08/2017"
    },
    {
        "id": "00000165710000000538",
        "nombre_comercial": "TOTALTRAVEL",
        "localizador": "266086",
        "referencia_agencia": "CONAMPLIACIONES",
        "total_asegurados": "1",
        "estado": "baja",
        "asegurados": [
            {
                "numero_documento": "57642990E",
                "nombre": "Jon",
                "apellidos": "Doe"
            }
        ],
        "fecha_contratacion": "02/08/2017"
    },
    {
        "id": "00000165710000000539",
        "nombre_comercial": "TOTALTRAVEL",
        "localizador": "266087",
        "referencia_agencia": "CONAMPLIACIONES",
        "total_asegurados": "1",
        "estado": "baja",
        "asegurados": [
            {
                "numero_documento": "57642990E",
                "nombre": "Jon",
                "apellidos": "Doe"
            }
        ],
        "fecha_contratacion": "02/08/2017"
    }
    ...
]
```
<br />

**Cabeceras de la respuesta**

Si analizamos bien las cabeceras de la respuesta del servicio podemos comprobar que revoca la cookie anterior y establece una cookie nueva.

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />