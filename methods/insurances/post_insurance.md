# Contratar un seguro

Permite contratar un seguro previamente tarificado y configurado.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
POST https://api.intermundial.com/v3/{locale}/seguros.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
<br />

**Cuerpo de la solicitud**

```json
{
   "referencia_agencia":"11111111H",
   "precio_base":271.95,
   "precio_ampliaciones":63.95,
   "precio_total":335.9,
   "poliza":{
        "id": "13700"
   },
   "asegurados":[
      {
         "tratamiento":"Sr.",
         "nombre":"Joe",
         "apellidos":"Doe",
         "tipo_documento":"NIF",
         "numero_documento":"57642990E",
         "fecha_nacimiento":"1992-03-23",
         "email":"info@intermundial.es",
         "direccion":"C/Irun, 7, 1º",
         "localidad":"Madrid",
         "provincia":"Madrid",
         "pais":{
               "id": 317,
               "nombre": "España",
               "continente": "Southern Europe",
               "capital": "Madrid",
               "codigo_pais": "ESP",
               "codigo_pais_alpha2": "ES"
         },
         "codigo_postal":"28008",
         "telefono":"9546789425",
         "is_principal":true
      },
      {
         "tratamiento":"Sr.",
         "nombre":"Manolo ",
         "apellidos":"Tena",
         "tipo_documento":"NIF",
         "numero_documento":"16321469W",
         "fecha_nacimiento":"1985-05-03",
         "email":"manolotena@gmail.com",
         "is_principal":false
      }
   ],
   "coberturas_ampliadas":[
    {
      "id_cruce": "200000070989",
      "id_poliza":"13700",
      "tipo":"incluida-lista",
      "valores": [
        {
          "id": "6272",
          "prima": "21.06",
          "limite": "2300"
        }
      ]
    },
    {
      "id_cruce": "200000070950",
      "id_poliza":"13700",
      "tipo": "incluida-lista",
      "valores": [
        {
          "id": "6276",
          "limite": "4000",
          "prima": "29.28"
        }
      ]
    },
    {
      "id_cruce": "200000070958",
      "id_poliza":"13700",
      "tipo": "incluida-lista",
      "valores": [
        {
          "id": "6283",
          "limite": "60000",
          "prima": "13.61"
        }
      ]
    }
   ],
   "fecha_inicio":"2017-07-28",
   "fecha_fin":"2017-07-31",
   "total_asegurados":"2",
   "pais_origen":{
               "id": 317,
               "nombre": "España",
               "continente": "Southern Europe",
               "capital": "Madrid",
               "codigo_pais": "ESP",
               "codigo_pais_alpha2": "ES"
   },
   "pais_destino":{
               "id": 317,
               "nombre": "España",
               "continente": "Southern Europe",
               "capital": "Madrid",
               "codigo_pais": "ESP",
               "codigo_pais_alpha2": "ES"
   },
   "importe_asegurado_valor":null,
   "parametros": [
       {
         "id_parametro": 245,
         "id_valor": 573
       },
       {
         "id_parametro": 246,
         "id_valor": 8428
       }
   ]
}
```
**Consideraciones**
Para los objetos de país, la única propiedad requerida es `codigo_pais`. 

<br />

**Solicitud CURL**

```
curl -X POST \
  https://api.intermundial.com/v3/es/seguros \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
   "id":null,
   "localizador":null,
   "referencia_agencia":"11111111H",
   "fecha_caducidad":null,
   "precio_base":271.95,
   "precio_ampliaciones":63.95,
   "precio_total":335.9,
   "poliza":{
        "id": "13700"
   },
   "asegurados":[
      {
         "tratamiento":"Sr.",
         "nombre":"Joe",
         "apellidos":"Doe",
         "tipo_documento":"NIF",
         "numero_documento":"57642990E",
         "fecha_nacimiento":"1992-03-23",
         "email":"info@intermundial.es",
         "direccion":"C/Irun, 7, 1º",
         "localidad":"Madrid",
         "provincia":"Madrid",
         "pais":{
               "id": 317,
               "nombre": "España",
               "continente": "Southern Europe",
               "capital": "Madrid",
               "codigo_pais": "ESP",
               "codigo_pais_alpha2": "ES"
         },
         "codigo_postal":"28008",
         "telefono":"9546789425",
         "is_principal":true
      },
      {
         "tratamiento":"Sr.",
         "nombre":"Manolo ",
         "apellidos":"Tena",
         "tipo_documento":"NIF",
         "numero_documento":"16321469W",
         "fecha_nacimiento":"1985-05-03",
         "email":"manolotena@gmail.com",
         "is_principal":false
      }
   ],
   "coberturas_ampliadas":[
    {
      "id_cruce": "200000070989",
      "id_poliza":"13700",
      "tipo":"incluida-lista",
      "valores": [
        {
          "id": "6272",
                "prima": "21.06",
                "limite": "2300"
        }
      ]
    },
    {
      "id_cruce": "200000070950",
      "id_poliza":"13700",
      "tipo": "incluida-lista",
      "valores": [
        {
        "id": "6276",
        "limite": "4000",
        "prima": "29.28"
        }
      ]
    },
    {
      "id_cruce": "200000070958",
      "id_poliza":"13700",
      "tipo": "incluida-lista",
      "valores": [
        {
        "id": "6283",
        "limite": "60000",
        "prima": "13.61"
        }
      ]
    }
   ],
   "certificado_con_pvp_path":null,
   "certificado_sin_pvp_path":null,
   "fecha_inicio":"2017-07-28",
   "fecha_fin":"2017-07-31",
   "total_asegurados":"2",
   "pais_origen":{
               "id": 317,
               "nombre": "España",
               "continente": "Southern Europe",
               "capital": "Madrid",
               "codigo_pais": "ESP",
               "codigo_pais_alpha2": "ES"
         },
   "pais_destino":{
               "id": 317,
               "nombre": "España",
               "continente": "Southern Europe",
               "capital": "Madrid",
               "codigo_pais": "ESP",
               "codigo_pais_alpha2": "ES"
         },
   "importe_asegurado_valor":null,
   "parametros": [
       {
         "id_parametro": 245,
         "id_valor": 573
       },
       {
         "id_parametro": 246,
         "id_valor": 8428
       }
   ],
   "fecha_contratacion":null
}'
```
<br />

## Respuesta con éxito

Devuelve el id del seguro para confirmar la creación del mismo.

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **id**: Identificador del seguro. Permite trabajar en futuras llamadas relacionadas con el seguro.
<br />

**Cuerpo de la respuesta**

```json
{
    "id": "00000137000000000817"
}
```
<br />

**Cabeceras de la respuesta**

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />