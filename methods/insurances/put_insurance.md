# Modificar un seguro

Permite modificar un seguro previamente contratado. Las modificaciones solo seran efectivas si se realizan como muy tarde un dia antes de la fecha de inicio del viaje.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
PUT https://api.intermundial.com/v3/{locale}/seguros/{id_seguro}.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
id_seguro | string | Identificador unico del seguro |
<br />

**Cuerpo de la solicitud**

```json
{
   "referencia_agencia":"11111111H",
   "precio_base":453.29,
   "precio_ampliaciones":87.11,
   "precio_total":540.4,
   "poliza":{
      "id":"13700"
   },
   "asegurados":[
      {
         "tratamiento":"Sr.",
         "nombre":"Marius Claudiu",
         "apellidos":"Spalatelu",
         "tipo_documento":"NIE",
         "numero_documento":"X8804321J",
         "fecha_nacimiento":"1992-03-23",
         "email":"mclaudiu@intermundial.es",
         "direccion":"Calle Real 15",
         "localidad":"Madrid",
         "provincia":"Madrid",
         "pais":"317",
         "codigo_postal":"28009",
         "telefono":"669696969",
         "is_principal":true
      }
   ],
   "coberturas_ampliadas":[
      {
         "id_cruce":"200000070883",
         "id_poliza":"13700",
         "tipo":"incluida-lista",
         "valores":[
            {
               "id":"6288",
               "is_incluida":false,
               "prima":"35.14",
               "limite":"4100"
            }
         ]
      },
      {
         "id_cruce":"200000070844",
         "id_poliza":"13700",
         "tipo":"incluida-lista",
         "valores":[
            {
               "id":"6291",
               "is_incluida":false,
               "prima":"29.28",
               "limite":"6000"
            }
         ]
      },
      {
         "id_cruce":"200000070852",
         "id_poliza":"13700",
         "tipo":"incluida-lista",
         "valores":[
            {
               "id":"6299",
               "is_incluida":false,
               "prima":"22.69",
               "limite":"80000"
            }
         ]
      }
   ],
   "fecha_inicio":"2017-08-18",
   "fecha_fin":"2017-08-31",
   "total_asegurados":"1",
   "pais_destino":272,
   "importe_asegurado_valor":null,
   "parametros":[
      {
         "id_parametro":"245",
         "id_valor":"575"
      },
      {
         "id_parametro":"246",
         "id_valor":"8428"
      }
   ]
}
```

<br />

**Solicitud CURL**

```
curl -X PUT \
  https://api.intermundial.com/v3/es/seguros/00000137000000000817 \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 98e9cd78-285c-ae08-4c46-68b258e3b385' \
  -d '{
   "referencia_agencia":"11111111H",
   "precio_base":453.29,
   "precio_ampliaciones":87.11,
   "precio_total":540.4,
   "poliza":{
      "id":"13700"
   },
   "asegurados":[
      {
         "tratamiento":"Sr.",
         "nombre":"Marius Claudiu",
         "apellidos":"Spalatelu",
         "tipo_documento":"NIE",
         "numero_documento":"X8804321J",
         "fecha_nacimiento":"1992-03-23",
         "email":"mclaudiu@intermundial.es",
         "direccion":"Calle Real 15",
         "localidad":"Madrid",
         "provincia":"Madrid",
         "pais":"317",
         "codigo_postal":"28009",
         "telefono":"669696969",
         "is_principal":true
      }
   ],
   "coberturas_ampliadas":[
      {
         "id_cruce":"200000070883",
         "id_poliza":"13700",
         "tipo":"incluida-lista",
         "valores":[
            {
               "id":"6288",
               "is_incluida":false,
               "prima":"35.14",
               "limite":"4100"
            }
         ]
      },
      {
         "id_cruce":"200000070844",
         "id_poliza":"13700",
         "tipo":"incluida-lista",
         "valores":[
            {
               "id":"6291",
               "is_incluida":false,
               "prima":"29.28",
               "limite":"6000"
            }
         ]
      },
      {
         "id_cruce":"200000070852",
         "id_poliza":"13700",
         "tipo":"incluida-lista",
         "valores":[
            {
               "id":"6299",
               "is_incluida":false,
               "prima":"22.69",
               "limite":"80000"
            }
         ]
      }
   ],
   "fecha_inicio":"2017-08-18",
   "fecha_fin":"2017-08-31",
   "total_asegurados":"1",
   "pais_destino":272,
   "importe_asegurado_valor":null,
   "parametros":[
      {
         "id_parametro":"245",
         "id_valor":"575"
      },
      {
         "id_parametro":"246",
         "id_valor":"8428"
      }
   ]
}'
```
<br />

## Respuesta con éxito

Devuelve el id del seguro para confirmar la modficación del mismo.

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **id**: Identificador del seguro. Permite trabajar en futuras llamadas relacionadas con el seguro.
<br />

**Cuerpo de la respuesta**

```json
{
    "id": "00000137000000000817"
}
```
<br />

**Cabeceras de la respuesta**

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />