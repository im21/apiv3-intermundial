# Listado de polizas contratables

Obtiene el listado de pólizas que el usuario tiene disponibles para contratar.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/es/polizas.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
<br />

**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/polizas \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

Devuelve un arreglo con información parcial de las pólizas.

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **id**: Identificador único de la póliza. Se solicita en futuras llamadas para tarificar, contratar, etc...
* **numero**: El numero de póliza.
* **nombre_comercial**: Nombre comercial de la póliza.
* **logo_path**: URL del logo del producto. Si no hay ningun logo especifico para un producto, se carga un logo por defecto.
* **background_path**: URL de la imagen de fondo del producto. Si no hay ninguna imagen de fondo especifica para un producto, se carga una por defecto.
<br />

**Cuerpo de la respuesta**

```json
[
    {
        "id": "17029",
        "numero": "MMC-PRUEBA9",
        "nombre_comercial": "PRUEBA PRIMA FIJA",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "16571",
        "numero": "MMC-970078",
        "nombre_comercial": "TOTALTRAVEL",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "15800",
        "numero": "MMC-PRUEBA8",
        "nombre_comercial": "PRUEBA MODALIDAD-DESTINO",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "15799",
        "numero": "MMC-PRUEBA7",
        "nombre_comercial": "PRUEBA DURACION-EDAD",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "15465",
        "numero": "MMC-PRUEBA6",
        "nombre_comercial": "PRUEBA DURACION/IMPORTE",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "15464",
        "numero": "MMC-PRUEBA5",
        "nombre_comercial": "PRUEBA DESTINO/IMPORTE",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "15463",
        "numero": "MMC-PRUEBA4",
        "nombre_comercial": "PRUEBA IMPORTE",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "15348",
        "numero": "MMC-PRUEBA3",
        "nombre_comercial": "PRUEBA PORCENTAJE",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "15347",
        "numero": "MMC-PRUEBA2",
        "nombre_comercial": "PRUEBA MODALIDAD-IMPORTE",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "15319",
        "numero": "MMC-PRUEBA1",
        "nombre_comercial": "PRUEBA DESTINO-DURACION",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "13700",
        "numero": "MMC-970041",
        "nombre_comercial": "Totaltravel annual",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "13696",
        "numero": "MMC-970039",
        "nombre_comercial": "TOTALTRAVEL MINI WEB",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "13690",
        "numero": "MMC-970037",
        "nombre_comercial": "WINTERSPORTS BASIC",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "13680",
        "numero": "MMC-970035",
        "nombre_comercial": "WINTERSPORTS WEB",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "13665",
        "numero": "MMC-970033",
        "nombre_comercial": "TOTALSPORTS WEB",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "12824",
        "numero": "MMC-970027",
        "nombre_comercial": "GO GROUP WEB",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "12816",
        "numero": "MMC-970025",
        "nombre_comercial": "GO SCHENGEN WEB",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "12814",
        "numero": "MMC-970023",
        "nombre_comercial": "AM M GO CANCELLATION",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/AM%20M%20GO%20CANCELLATION/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/AM%20M%20GO%20CANCELLATION/background.jpg"
    },
    {
        "id": "12777",
        "numero": "MMC-970015",
        "nombre_comercial": "TOTALTRAVEL",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "12347",
        "numero": "07622000398-WA",
        "nombre_comercial": "STUDY PLUS WEB ERV",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/STUDY%20PLUS%20WEB%20ERV/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/STUDY%20PLUS%20WEB%20ERV/background.jpg"
    },
    {
        "id": "11848",
        "numero": "1-26-5252371-A",
        "nombre_comercial": "PET PLUS",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg"
    },
    {
        "id": "7873",
        "numero": "55-0684139",
        "nombre_comercial": "CRUCEROS WEB",
        "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/CRUCEROS%20WEB/logo.png",
        "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/CRUCEROS%20WEB/background.jpg"
    }
]
```
<br />

**Cabeceras de la respuesta**

Si analizamos bien las cabeceras de la respuesta del servicio podemos comprobar que revoca la cookie anterior y establece una cookie nueva.

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />