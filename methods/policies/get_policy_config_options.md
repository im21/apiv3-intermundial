# Opciones de configuración de una póliza

Consulta los posibles valores de configuración de una póliza
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/es/polizas/{id_poliza}/opciones.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
id_poliza | string | Identificador unico de la póliza que se quiere consultar |
<br />

**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/polizas/12777/opciones \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

Devuelve un objeto con la información de las posibles opciones de configuración, el tipo de tarifa y validaciones dinamicas.

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **opciones**: Arreglo que contiene los diferentes parametros y los posibles valores que se pueden seleccionar para la póliza consultada.
* **validaciones**: Validaciones especiales.
* **tipo_tarifa**: El tipo de tarifa es una clasificación. Permite saber la conbinación de los parametros esperada.
<br />

**Cuerpo de la respuesta**

```json
{
    "opciones": [
        {
            "id": "41",
            "numero_parametro": 0,
            "nombre": "Destino",
            "tipo": "ambito_cobertura",
            "valores": [
                {
                    "id": "41",
                    "nombre": "Local",
                    "desde": "0",
                    "hasta": "0"
                },
                {
                    "id": "43",
                    "nombre": "Mundial",
                    "desde": "0",
                    "hasta": "0"
                },
                {
                    "id": "42",
                    "nombre": "Continental",
                    "desde": "0",
                    "hasta": "0"
                }
            ]
        },
        {
            "id": "42",
            "numero_parametro": 1,
            "nombre": "Duración",
            "tipo": "duracion",
            "valores": [
                {
                    "id": "44",
                    "nombre": "De 1 a 5 días",
                    "desde": "1",
                    "hasta": "5"
                },
                {
                    "id": "45",
                    "nombre": "De 6 a 9 ",
                    "desde": "6",
                    "hasta": "9"
                },
                {
                    "id": "46",
                    "nombre": "De 10 a 16 ",
                    "desde": "10",
                    "hasta": "16"
                },
                {
                    "id": "47",
                    "nombre": "De 17 a 22 ",
                    "desde": "17",
                    "hasta": "22"
                },
                {
                    "id": "48",
                    "nombre": "De 23 a 34 ",
                    "desde": "23",
                    "hasta": "34"
                },
                {
                    "id": "49",
                    "nombre": "De 35 a 43 ",
                    "desde": "35",
                    "hasta": "43"
                },
                {
                    "id": "50",
                    "nombre": "De 44 a 68 ",
                    "desde": "44",
                    "hasta": "68"
                },
                {
                    "id": "51",
                    "nombre": "De 69 a 95 días",
                    "desde": "69",
                    "hasta": "95"
                }
            ]
        }
    ],
    "validaciones": {
        "viaje": {
            "total_asegurados": {
                "requerido": null,
                "minimo": "1",
                "maximo": "5",
                "campo_referencia": "total_asegurados",
                "valor": null
            },
            "fecha_inicio": {
                "requerido": null,
                "valor": "1",
                "minimo": null,
                "maximo": null,
                "campo_referencia": "fecha_actual"
            },
            "fecha_modificacion": {
                "requerido": null,
                "valor": "-1",
                "campo_referencia": "fecha_inicio"
            },
            "fecha_anulacion": {
                "requerido": null,
                "valor": "-1",
                "campo_referencia": "fecha_inicio"
            }
        }
    },
    "tipo_tarifa": "DESTINO_DURACION"
}
```
<br />

**Cabeceras de la respuesta**

Si analizamos bien las cabeceras de la respuesta del servicio podemos comprobar que revoca la cookie anterior y establece una cookie nueva.

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />