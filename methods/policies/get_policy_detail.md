# Detalle de una póliza

Permite consultar la información ampliada y completa de una póliza.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/es/polizas/{id_poliza}.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
id_poliza | string | Identificador unico de la póliza que se quiere consultar |
<br />

**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/polizas \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

Devuelve un objeto con la información util de la póliza.

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **id**: Identificador único de la póliza. Se solicita en futuras llamadas para tarificar, contratar, etc...
* **numero**: El numero de póliza.
* **nombre_comercial**: Nombre comercial de la póliza.
* **descripcion**: Descripción del producto.
* **informacion_comercial**: Información comercial de la póliza.
* **logo_path**: URL del logo del producto. Si no hay ningun logo especifico para un producto, se carga un logo por defecto.
* **background_path**: URL de la imagen de fondo del producto. Si no hay ninguna imagen de fondo especifica para un producto, se carga una por defecto.
* **resumen_path**: URL de descarga del resumen del seguro.
* **resumen_sin_precio_path**: URL de descarga del resumen sin precios del seguro.
* **condicionado_path**: URL de descarga del condicionado general del seguro.
<br />

**Cuerpo de la respuesta**

```json
{
    "id": "12777",
    "numero": "MMC-970015",
    "comision_agencia": "10",
    "nombre_comercial": "TOTALTRAVEL",
    "descripcion": "Para viajes de hasta 95 días. El más completo, personalizable y con mejores límites. ",
    "informacion_comercial": [
        "Hasta 100.000 € en gastos médicos",
        "34 garantías de anulación",
        "Hasta 2.100 € en cobertura de equipajes",
        "Ampliaciones de Deportes y Mascotas",
        "esto es una prueba fran",
        "Para viajes de hasta 95 días. El más completo, personalizable y con mejores límites. "
    ],
    "logo_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/logo.png",
    "background_path": "https://api.intermundial.com/v3/bundles/intermundialwebservice/images/productos/DEFAULT/background.jpg",
    "resumen_path": "",
    "resumen_sin_precio_path": "",
    "condicionado_path": ""
}
```
<br />

**Cabeceras de la respuesta**

Si analizamos bien las cabeceras de la respuesta del servicio podemos comprobar que revoca la cookie anterior y establece una cookie nueva.

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />