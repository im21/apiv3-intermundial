# Detalle de un documento

Permite descargar un documento asociado a una poliza.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/es/polizas/{id_poliza}/documentos/{id_documento}.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
id_poliza | string | Identificador unico de la póliza |
id_documento | string | Identificador unico del documento que se quiere descargar |
<br />

**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/polizas/15799/documentos/2802955 \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

Retorna un documento en descarga. El codigo de estado devuelto por la llamada es `200 OK`. El contenido de la respuesta esta vacio puesto que es un documento descargado.

<br />

**Cuerpo de la respuesta**

```json
```
<br />

**Cabeceras de la respuesta**

```
Accept-Ranges bytes
Cache-Control public, max-age=2592000
Connection Keep-Alive
Content-Length 461551
Content-Type application/pdf
Date Thu, 31 Aug 2017 08:28:47 GMT
Expires Sat, 30 Sep 2017 08:28:47 GMT
Keep-Alive timeout=5, max=100
Last-Modified Thu, 31 Aug 2017 08:28:50 GMT
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=3crtqr18s70m7fkgb4losqilu2; expires=Thu, 31-Aug-2017 10:28:47 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />