# Listado de documentos de una póliza

Permite consultar el listado de documentos descargables de una póliza.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/es/polizas/{id_poliza}/documentos.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
id_poliza | string | Identificador unico de la póliza que se quiere consultar |
<br />

**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/polizas/15799/documentos \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

Devuelve un arreglo con la información necesaria para poder descargar el documento mas adelante.

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **id**: Identificador único del documento. Se usa en la llamada de [detalle de un documento](get_policy_document_detail.md) para descargar el documento.
* **fecha_creacion**: Fecha en la que se cargo el documento en el sistema.
* **categoria**: Categoria para poder identificar de que tipo de documento estamos hablando. (Condicionado, resumen, resumen sin precio, etc...)
<br />

**Cuerpo de la respuesta**

```json
[
    {
        "id": "2802955",
        "fecha_creacion": "13/03/2017",
        "categoria": "Resumen sin precio"
    },
    {
        "id": "2802980",
        "fecha_creacion": "13/03/2017",
        "categoria": "Background"
    },
    {
        "id": "2802982",
        "fecha_creacion": "13/03/2017",
        "categoria": "Logo"
    },
    {
        "id": "2789047",
        "fecha_creacion": "23/11/2016",
        "categoria": "Condicionado"
    },
    {
        "id": "2789046",
        "fecha_creacion": "23/11/2016",
        "categoria": "Resumen"
    }
]
```
<br />

**Cabeceras de la respuesta**

Si analizamos bien las cabeceras de la respuesta del servicio podemos comprobar que revoca la cookie anterior y establece una cookie nueva.

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />