# Obtener precios de una póliza

Obtiene el precio de un seguro, segun las opciones de configuracion seleccionadas.
<br />
<br />
Para obtener las posibles opciones de configuración consultar [aqui.](get_policy_config_options.md)
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/es/polizas/{id_poliza}/precios.{?format}?parametros[0][id_parametro]=41&parametros[0][id_valor]=42&parametros[1][id_parametro]=42&parametros[1][id_valor]=48
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
id_poliza | integer | Identificador unico de la póliza que se quiere consultar |
parametros[n][id_parametro] | integer | Identificador unico del parametro devuelto en la [llamada](get_policy_config_options.md) de obtencion de opciones de configuracion
parametros[n][id_valor] | integer | Identificador unico del valor seleccionado devuelto en la [llamada](get_policy_config_options.md) de obtencion de opciones de configuracion para el parametro indicado en la opcion anterior.
<br />

**Solicitud CURL**

```
curl -X GET \
  'https://api.intermundial.com/v3/es/polizas/12777/precios?parametros%5B0%5D%5Bid_parametro%5D=41&parametros%5B0%5D%5Bid_valor%5D=42&parametros%5B1%5D%5Bid_parametro%5D=42&parametros%5B1%5D%5Bid_valor%5D=48' \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

Devuelve un objeto con la información de las posibles opciones de configuración, el tipo de tarifa y validaciones dinamicas.

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **precio_base**: Precio basico del seguro sin condiciones especiales.
* **is_tarifa_por_asegurado**: Si es `true` el precio base y la suma del precio de las ampliaciones tiene que multipliarse por el numero de asegurados.
* **coberturas_ampliables**: Arreglo de coberturas ampliables con la información necesaria para contratar un seguro. 
<br />

**Cuerpo de la respuesta**

```json
{
    "precio_base": 77.4,
    "is_tarifa_por_asegurado": true,
    "coberturas_ampliables": [
        {
            "id": "10219",
            "id_cruce": "200000039317",
            "nombre": "Gastos Médicos",
            "descripcion": "Ampliar la cobertura de gastos médicos es la elección más acertada si vas a practicar deportes de invierno fuera de España, ya que el coste de la atención sanitaria suele ser más elevado. El límite que selecciones te cubrirá tanto los gastos del tratamiento como de los profesionales que te atiendan en el extranjero.",
            "tipo": "incluida-lista",
            "valores": [
                {
                    "id": "2230",
                    "is_incluida": true,
                    "prima": 0,
                    "limite": 50000
                },
                {
                    "id": "2231",
                    "is_incluida": false,
                    "prima": 14.96,
                    "limite": 60000
                },
                {
                    "id": "2232",
                    "is_incluida": false,
                    "prima": 26.63,
                    "limite": 70000
                },
                {
                    "id": "2233",
                    "is_incluida": false,
                    "prima": 39.96,
                    "limite": 80000
                },
                {
                    "id": "2234",
                    "is_incluida": false,
                    "prima": 54.61,
                    "limite": 100000
                },
                {
                    "id": "2235",
                    "is_incluida": false,
                    "prima": 67.13,
                    "limite": 150000
                }
            ]
        },
        {
            "id": "10381",
            "id_cruce": "200000039460",
            "nombre": "Deportes",
            "descripcion": "Con esta ampliación, disfrutarás de un extra de tranquilidad en las actividades deportivas que practiques de forma esporádica (máx. 2 días) durante tu viaje (safari, buceo, surf, vuelo en helicóptero...). Más de 30 actividades cubiertas.",
            "tipo": "opcional-unica",
            "valores": [
                {
                    "id": "1",
                    "is_incluida": false,
                    "prima": 19.5,
                    "limite": 3000
                }
            ]
        },
        {
            "id": "10257",
            "id_cruce": "200000039334",
            "nombre": "Equipajes",
            "descripcion": "Si el valor de los objetos que llevas en tu equipaje supera el límite que lleva por defecto el seguro, amplía esta cobertura a tu gusto hasta conseguir la máxima protección. Recuerda que el material de fotografía, imagen y sonido, queda cubierto hasta el 50% del límite que selecciones.",
            "tipo": "incluida-lista",
            "valores": [
                {
                    "id": "2236",
                    "is_incluida": true,
                    "prima": 0,
                    "limite": 1300
                },
                {
                    "id": "2237",
                    "is_incluida": false,
                    "prima": 23.4,
                    "limite": 2300
                },
                {
                    "id": "2238",
                    "is_incluida": false,
                    "prima": 39.04,
                    "limite": 3300
                },
                {
                    "id": "2239",
                    "is_incluida": false,
                    "prima": 52.06,
                    "limite": 4300
                }
            ]
        },
        {
            "id": "8034",
            "id_cruce": "200000039340",
            "nombre": "Anulación",
            "descripcion": "Esta ampliación es especialmente recomendable si el coste total de la reserva de tu viaje supera el límite que lleva por defecto el seguro. Con ello te garantizas poder recuperar los gastos de cancelación de los billetes de transporte, las reservas hoteleras y otras actividades que hayas contratado. Ahora no hay excusa para no tener los gastos de tu viaje asegurados al 100%.",
            "tipo": "incluida-lista",
            "valores": [
                {
                    "id": "2240",
                    "is_incluida": true,
                    "prima": 0,
                    "limite": 3000
                },
                {
                    "id": "2241",
                    "is_incluida": false,
                    "prima": 32.53,
                    "limite": 4000
                },
                {
                    "id": "2242",
                    "is_incluida": false,
                    "prima": 53.33,
                    "limite": 5000
                },
                {
                    "id": "2243",
                    "is_incluida": false,
                    "prima": 80.65,
                    "limite": 6000
                },
                {
                    "id": "2244",
                    "is_incluida": false,
                    "prima": 103.13,
                    "limite": 7000
                },
                {
                    "id": "2245",
                    "is_incluida": false,
                    "prima": 116.2,
                    "limite": 8000
                },
                {
                    "id": "2246",
                    "is_incluida": false,
                    "prima": 132.85,
                    "limite": 9000
                }
            ]
        },
        {
            "id": "9156",
            "id_cruce": "200000039423",
            "nombre": "Accidentes 24h",
            "descripcion": "Protege el futuro de tu pareja, familia o legado. La cobertura de accidentes es la forma más segura de garantizar una fuente de ingresos y mantener vuestro nivel de vida en caso de que te ocurriera algo durante tu viaje. El límite de edad para contratar la ampliación de Accidentes es de 70 años.",
            "tipo": "incluida-lista",
            "valores": [
                {
                    "id": "2251",
                    "is_incluida": true,
                    "prima": 0,
                    "limite": 10000
                },
                {
                    "id": "2252",
                    "is_incluida": false,
                    "prima": 15.12,
                    "limite": 40000
                },
                {
                    "id": "2253",
                    "is_incluida": false,
                    "prima": 25.22,
                    "limite": 60000
                },
                {
                    "id": "2254",
                    "is_incluida": false,
                    "prima": 43.31,
                    "limite": 110000
                }
            ]
        }
    ]
}
```
<br />

**Cabeceras de la respuesta**

Si analizamos bien las cabeceras de la respuesta del servicio podemos comprobar que revoca la cookie anterior y establece una cookie nueva.

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />