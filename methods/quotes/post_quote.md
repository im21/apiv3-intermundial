# Crear un presupuesto

Permite crear un presupuesto previamente tarificado.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
POST https://api.intermundial.com/v3/{locale}/presupuestos.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
<br />

**Cuerpo de la solicitud**

```json
{
  "id": null,
  "referencia_agencia": null,
  "precio_base": 40,
  "precio_ampliaciones": 0,
  "precio_total": 40,
  "poliza": {
    "id": "19221",
    "nombre_comercial": "DESTINO_DURACION",
    "tipo_tarifa": "DESTINO_DURACION"
  },
  "asegurados": [
    {
      "tratamiento": "Sr.",
      "nombre": "Weldon",
      "apellidos": "Linder",
      "tipo_documento": "NIF",
      "numero_documento": "45391130D",
      "fecha_nacimiento": "1986-08-17",
      "email": "weldonlin@gmail.com",
      "direccion": "Calle de Abril 18",
      "localidad": "Madrid",
      "provincia": "Madrid",
      "pais": {
        "id": 317,
        "nombre": "España",
        "continente": "Southern Europe",
        "capital": "Madrid",
        "codigo_pais": "ESP",
        "text": "España"
      },
      "codigo_postal": "28004",
      "telefono": "654056406",
      "is_principal": true
    },
    {
      "tratamiento": "Sr.",
      "nombre": "Tommie",
      "apellidos": "Mat",
      "tipo_documento": "NIE",
      "numero_documento": "Z9003189M",
      "fecha_nacimiento": "1982-10-25",
      "email": "tommiemat@gmail.com",
      "is_principal": false
    }
  ],
  "coberturas_ampliadas": [
    {
      "id": "10381",
      "id_cruce": "200000219772",
      "id_poliza": "19221",
      "nombre": "Deportes",
      "descripcion": null,
      "tipo": "opcional-unica",
      "valores": [
        {
          "id": "0",
          "is_incluida": true,
          "prima": null,
          "limite": null,
          "posicion": 0
        }
      ]
    },
    {
      "id": "10282",
      "id_cruce": "200000219773",
      "id_poliza": "19221",
      "nombre": "Rehabilitación",
      "descripcion": "Rehabilitación",
      "tipo": "opcional-lista",
      "valores": [
        {
          "id": "0",
          "is_incluida": true,
          "prima": null,
          "limite": null,
          "posicion": 0
        }
      ]
    },
    {
      "id": "8034",
      "id_cruce": "200000219775",
      "id_poliza": "19221",
      "nombre": "Anulación",
      "descripcion": "Gastos de anulación de viaje",
      "tipo": "incluida-lista",
      "valores": [
        {
          "id": "36151",
          "is_incluida": true,
          "prima": 0,
          "limite": 1000,
          "posicion": 0
        }
      ]
    }
  ],
  "fecha_inicio": "2018-05-08",
  "fecha_fin": "2018-05-18",
  "total_asegurados": "2",
  "pais_origen": {
    "id": 317,
    "nombre": "España",
    "continente": "Southern Europe",
    "capital": "Madrid",
    "codigo_pais": "ESP"
  },
  "pais_destino": {
    "id": 307,
    "nombre": "Alemania",
    "continente": "Western Europe",
    "capital": "Berlin",
    "codigo_pais": "DEU",
    "text": "Alemania"
  },
  "parametros": [
    {
      "id_parametro": "3626",
      "id_valor": "14947"
    },
    {
      "id_parametro": "3627",
      "id_valor": "14952"
    }
  ]
}
```

<br />

**Solicitud CURL**

```
curl -X POST \
  https://api.intermundial.com/v3/es/seguros \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
        "id": null,
        "referencia_agencia": null,
        "precio_base": 40,
        "precio_ampliaciones": 0,
        "precio_total": 40,
        "poliza": {
          "id": "19221",
          "nombre_comercial": "DESTINO_DURACION",
          "tipo_tarifa": "DESTINO_DURACION"
        },
        "asegurados": [
          {
            "tratamiento": "Sr.",
            "nombre": "Weldon",
            "apellidos": "Linder",
            "tipo_documento": "NIF",
            "numero_documento": "45391130D",
            "fecha_nacimiento": "1986-08-17",
            "email": "weldonlin@gmail.com",
            "direccion": "Calle de Abril 18",
            "localidad": "Madrid",
            "provincia": "Madrid",
            "pais": {
              "id": 317,
              "nombre": "España",
              "continente": "Southern Europe",
              "capital": "Madrid",
              "codigo_pais": "ESP",
              "text": "España"
            },
            "codigo_postal": "28004",
            "telefono": "654056406",
            "is_principal": true
          },
          {
            "tratamiento": "Sr.",
            "nombre": "Tommie",
            "apellidos": "Mat",
            "tipo_documento": "NIE",
            "numero_documento": "Z9003189M",
            "fecha_nacimiento": "1982-10-25",
            "email": "tommiemat@gmail.com",
            "is_principal": false
          }
        ],
        "coberturas_ampliadas": [
          {
            "id": "10381",
            "id_cruce": "200000219772",
            "id_poliza": "19221",
            "nombre": "Deportes",
            "descripcion": null,
            "tipo": "opcional-unica",
            "valores": [
              {
                "id": "0",
                "is_incluida": true,
                "prima": null,
                "limite": null,
                "posicion": 0
              }
            ]
          },
          {
            "id": "10282",
            "id_cruce": "200000219773",
            "id_poliza": "19221",
            "nombre": "Rehabilitación",
            "descripcion": "Rehabilitación",
            "tipo": "opcional-lista",
            "valores": [
              {
                "id": "0",
                "is_incluida": true,
                "prima": null,
                "limite": null,
                "posicion": 0
              }
            ]
          },
          {
            "id": "8034",
            "id_cruce": "200000219775",
            "id_poliza": "19221",
            "nombre": "Anulación",
            "descripcion": "Gastos de anulación de viaje",
            "tipo": "incluida-lista",
            "valores": [
              {
                "id": "36151",
                "is_incluida": true,
                "prima": 0,
                "limite": 1000,
                "posicion": 0
              }
            ]
          }
        ],
        "fecha_inicio": "2018-05-08",
        "fecha_fin": "2018-05-18",
        "total_asegurados": "2",
        "pais_origen": {
          "id": 317,
          "nombre": "España",
          "continente": "Southern Europe",
          "capital": "Madrid",
          "codigo_pais": "ESP"
        },
        "pais_destino": {
          "id": 307,
          "nombre": "Alemania",
          "continente": "Western Europe",
          "capital": "Berlin",
          "codigo_pais": "DEU",
          "text": "Alemania"
        },
        "parametros": [
          {
            "id_parametro": "3626",
            "id_valor": "14947"
          },
          {
            "id_parametro": "3627",
            "id_valor": "14952"
          }
        ]
      }'
```
<br />

## Respuesta con éxito

Devuelve el id del presupuesto para confirmar la creación del mismo.

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **id**: Identificador del presupuesto. Permite trabajar en futuras llamadas relacionadas con el seguro.
<br />

**Cuerpo de la respuesta**

```json
{
    "id": "00000137000000000817"
}
```
<br />

**Cabeceras de la respuesta**

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />