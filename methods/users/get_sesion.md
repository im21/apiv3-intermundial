# Obtener datos de la sesion

Llamada que devuelve un objeto con información sobre el usuario y la sesión activa.

<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/{locale}/usuarios/{id_usuario}/sesiones.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
id_usuario | integer | Identificador unico del usuario |
<br />

**Solicitud CURL**

```
curl -X GET \
  https://api.intermundial.com/v3/es/usuarios/27828/sesiones \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **token_sesion**: Token que nos permite autenticar y autorizar futuras llamadas a otros endpoints de la API. Su validez es de dos horas desde el momento de su recepción.
* **tipo_token**: El tipo de token generado.
* **tiempo_sesion**: Timestamp que representa la fecha de caducidad del token de acceso y de la sesion.
* **token_refresco(beta)**: Es un token especial que nos permite solicicar un nuevo token de sesion, en caso de que el anterior haya caducado. El token de refresco tiene un tiempo de expiración de un mes.
<br />

**Cuerpo de la respuesta**

```json
{
    "token_sesion": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MDQxMDY0OTEsImVuY3J5cHRlZF9zZXNzaW9uX2lkIjoiNWMwOTUwN2U5MTcyZGQyZjIxMTYwZmQzYTljY2IwM2QyMTFhYzRiNWI5NTFiY2M0Mzk5Y2UyMDA4OWM4YjExYiJ9.k77xDVM2rMhGNZQjEDd5J0MU5lu41HL8n_AkhC5TULg",
    "url": "https://api.intermundial.com/v3",
    "tiempo_sesion": 1504106491,
    "tipo_token": "Bearer",
    "token_refresco": "d5f76880a949fb41d6d34824caed3052868dc38e5085248b7f15a2663541d6fb",
    "usuario": {
        "id": "27828",
        "nombre": "Sandbox",
        "apellidos": "InterMundial",
        "tipo": "Colab. Externo-Admin",
        "email": "sandbox@intermundial.es"
    }
}
```
<br />

**Cabeceras de la respuesta**

Como se puede observar se recibe una cookie en las cabeceras con un tiempo de expiración de 7200 segundos.

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection close
Content-Length 2779
Content-Type application/json
Date Wed, 30 Aug 2017 11:35:18 GMT
Expires Fri, 29 Sep 2017 11:35:18 GMT
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=7snek4r3aqq1au75sm28smvr17; expires=Wed, 30-Aug-2017 13:35:18 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />