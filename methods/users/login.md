# Inicio de Sesión

Llamada que devuelve un objeto con información sobre el usuario y la sesión que se inicia.

La sesion de la API de InterMundial es dual. Esto quiere decir que se puede establecer una sesion por medio de cookie o en su defecto por medio de un token de acceso. Una vez realizado el proceso de login tanto la cookie como el token estarán disponibles en la respuesta del servicio.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
POST https://api.intermundial.com/v3/{locale}/usuarios/sesiones.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
<br />

**Cuerpo de la solicitud**
```
{
	"usuario": "sandbox@intermundial.es",
	"password": "1234567a"
}
```
<br />

**Solicitud CURL**

```
curl -X POST \
  https://api.intermundial.com/v3/es/usuarios/sesiones \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{"usuario": "sandbox@intermundial.es","password": "1234567a"}'
```
<br />

## Respuesta con éxito

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **token_sesion**: Token que nos permite autenticar y autorizar futuras llamadas a otros endpoints de la API. Su validez es de dos horas desde el momento de su recepción.
* **tipo_token**: El tipo de token generado.
* **tiempo_sesion**: Timestamp que representa la fecha de caducidad del token de acceso y de la sesion.
* **usuario.id**: Se solicita en varias llamadas para poder hacer modificaciones o consultas de la información del usuario.
* **token_refresco(beta)**: Es un token especial que nos permite solicicar un nuevo token de sesion, en caso de que el anterior haya caducado. El token de refresco tiene un tiempo de expiración de un mes.
<br />

**Cuerpo de la respuesta**

```json
{
    {
            "token_sesion":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1MDQwODk1MzEsImVuY3J5cHRlZF9zZXNzaW9uX2lkIjoiOWQwYzJiOTQ2ZTQ1ZmFkMTY0MzViNjA3NjI1ODhiZjY3NTEzZGE4YTE4NzVmNWVmOGViNzA0ZTQ2MTU2MDlhYyJ9.FlcS8Swyc4wxAHpjOoz63lX2iw6>Mt9VhqaxEJKUM9FE",
        "url": "https://api-test.intermundial.com/v3",
        "tiempo_sesion": 1504089531,
        "tipo_token": "Bearer",
        "token_refresco": "682b16ed2471ed3a8ca9d73fb13bdaadad48f078b203510489bb4b3e49af10392667729d0145186712dc0d6f9dab44ee7ca2fdc8c5df544ef68520d3ede3e5cfe77c54f3d4bc466d36e4a3c8988fba89",
        "usuario": {
            "id": "27828",
            "nombre": "Sandbox",
            "apellidos": "InterMundial",
            "tipo": "Colab. Externo-Admin",
            "email": "sandbox@intermundial.es"
        }
}
```
<br />

**Cabeceras de la respuesta**

Como se puede observar se recibe una cookie en las cabeceras con un tiempo de expiración de 7200 segundos.

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection close
Content-Length 2779
Content-Type application/json
Date Wed, 30 Aug 2017 11:35:18 GMT
Expires Fri, 29 Sep 2017 11:35:18 GMT
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=7snek4r3aqq1au75sm28smvr17; expires=Wed, 30-Aug-2017 13:35:18 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />