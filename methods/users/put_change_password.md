# Cambiar contraseña

Si se realizo la llamada de reseteo de password se recibe por correo un enlace de restablecimiento de contraseña.
El enlace lleva implicito un hash que usado en esta llamada permite cambiar de manera efectiva la clave de acceso de la cuenta de usuario.
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
GET https://api.intermundial.com/v3/{locale}/usuarios/27828/password
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
<br />

**Parametros de la solicitud**

Name | Type | Description |
--- | --- | --- |
email_usuario | string | Email válido que se encuentre asociado a un usuario del sistema |
<br />

**Solicitud CURL**

```
curl -X GET \
  'https://api.intermundial.com/v3/es/usuarios/atributo/password?email=sandbox%40intermundial.es' \
  -H 'cache-control: no-cache'
```
<br />

## Respuesta con éxito

La respuesta de esta llamada no tiene contenido, devuelve un estado HTTP `204 No Content`

**Cuerpo de la respuesta**

```json
```
<br />

**Cabeceras de la respuesta**

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />