# Insertar llamada en la cola de llamadas salientes

Permite insertar una retrollama en la cola de salida indicada
<br />
<br />

## Información de la llamada

**Endpoint de la llamada**
```
POST https://api.intermundial.com/v3/{locale}/llamadas.{?format}
```
<br />
  
**Parámetros de la url**

Name | Type | Description |
--- | --- | --- |
locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
format | string | Formato del salida del documento [json, xml] |
<br />

**Cuerpo de la solicitud**

```json
{
  "nombre":"Marius",
  "telefono":"666666666",
  "horario":"18:19",
  "servicio_id":"189" // El id del servicio tiene que ser proporcionado por nuestro departamento de informatica.
}
```

<br />

**Solicitud CURL**

```
curl -X POST \
  https://api.intermundial.com/v3/es/seguros \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
        "nombre":"Marius",
        "telefono":"666666666",
        "horario":"18:19",
        "servicio_id":"189"
      }'
```
<br />

## Respuesta con éxito

Devuelve el id del seguro para confirmar la creación del mismo.

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* **id**: Identificador del seguro. Permite trabajar en futuras llamadas relacionadas con el seguro.
<br />

**Cuerpo de la respuesta**

```json
{
    "guardado": true
}
```
<br />

**Cabeceras de la respuesta**

```
Access-Control-Allow-Credentials true
Access-Control-Allow-Headers content-type
Access-Control-Allow-Headers authorization
Access-Control-Allow-Methods GET,POST,PUT,DELETE,OPTIONS
Access-Control-Allow-Origin 1
Access-Control-Max-Age 3600
Cache-Control no-cache, private, max-age=2592000
Connection Keep-Alive
Content-Length 561
Content-Type application/json
Date Wed, 30 Aug 2017 14:17:21 GMT
Expires Fri, 29 Sep 2017 14:17:21 GMT
Keep-Alive timeout=5, max=100
Server Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Set-Cookie intm_session=l2egoe0sm9krjr82g5nrtanef6; expires=Wed, 30-Aug-2017 16:17:21 GMT; Max-Age=7200; path=/; HttpOnly
Strict-Transport-Security max-age=31536000; preload
```
<br />
<br />