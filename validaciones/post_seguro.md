# Validaciones de creación y modificación de seguros

A continuacion se van a definir las validacaciones y tipos de datos necesarios para contratar o modificar un seguro.

<br />

## Contratación de un seguro

A continuación se defininen los diferentes campos utilizados en la llamada de contratacion de seguros y su tipo.

<br />

Nombre del campo | Requerido | Tipo | Validaciones adicionales | Longitud Maxima | Longitud Minima | Observaciones |
---|---|---|---| --- | --- | --- |
referencia_agencia | No | Cadena | No | No | 1 | Referencia externa de la agencia.
fecha_inicio | Si | Cadena | No | No | 1 | Fecha en formato YYYY-MM-DD del 1900-01-01 a la fecha actual
fecha_fin | Si | Cadena | No | No | 1 | Fecha en formato YYYY-MM-DD del 1900-01-01 a la fecha actual
total_asegurados | Si | Entero | No | No | 1 | Libre
precio_base | Si | Decimal | No | 13 | 1 | La separacion de la parte decimal se hace con "."
precio_ampliaciones | Si | Decimal | No | 13 | 1 | La separacion de la parte decimal se hace con "."
precio_total | Si | Decimal | No | 13 | 1 | La separacion de la parte decimal se hace con "."
poliza.id | Si | Entero | No | No | 1 | Libre
pais_origen.codigo_pais | No | Cadena | No | 3 | 4 | Codigo de pais obtenido del servicio de paises de la API
pais_destino.codigo_pais | No | Cadena | No | 3 | 4 | Codigo de pais obtenido del servicio de paises de la API
asegurados.*.tratamiento | No | Cadena | No | No | 1 | Libre
asegurados.*.nombre | Si | Cadena | No | 30 | 1 | Libre
asegurados.*.apellidos | Si | Cadena | No | 30 | 1 | Libre
asegurados.*.tipo_documento | No | Cadena | No | No | 1 | IT/CF - NIF - CIF - NIE - Comunitario - Resto del mundo - Menor sin NIF
asegurados.*.numero_documento | No | Cadena | No | No | 1 | Debe satisfacer el algoritmo oficial de validacion
asegurados.*.fecha_nacimiento | No | Cadena | No | No | 1 | Fecha en formato YYYY-MM-DD del 1900-01-01 a la fecha actual
asegurados.*.email | No | Cadena | No | 60 | 1 | Formato de email estandar email@domain.info
asegurados.*.direccion | No | Cadena | No | No | 1 | Libre
asegurados.*.localidad | No | Cadena | No | 50 | 1 | Libre
asegurados.*.provincia | No | Cadena | No | 50 | 1 | Libre
asegurados.*.pais.codigo_pais | Si | Cadena | No | 3 | 4 | Codigo de pais obtenido del servicio de paises de la API
asegurados.*.codigo_postal | No | Cadena | No | 20 | 1 | Libre
asegurados.*.telefono | No | Entero | No | No | 1 | Libre
asegurados.*.is_principal | No | Booleano | No | No | No | Solo puede tomar los valores true o false
coberturas_ampliadas.*.id_poliza | Si | Cadena | Id Unico | No | 1 | Id de la póliza que se desea contratar
coberturas_ampliadas.*.id_cruce | Si | Entero | Id Unico | No | 1 | La ampliacion no puede aparecer más de una vez en el listado de garantías enviado.
coberturas_ampliadas.*.tipo | Si | Cadena | No | No | 1 | incluida-lista - opcional-lista - opcional-unica
coberturas_ampliadas.*.tipo.valores.*.limite | Si | Entero | No | No | 1 | Libre
coberturas_ampliadas.*.tipo.valores.*.prima | Si | Entero | No | No | 1 | Libre
parametros.*.id_parametro | Si | Entero | No | No | 1 | Libre
parametros.*.id_valor | Si | Entero | No | No | 1 | Libre


```json
{
   "precio_base":19.48,
   "precio_ampliaciones":14.96,
   "precio_total":34.44,
   "poliza":{
        "id": 16571
   },
   "asegurados":[
      {
         "tratamiento":"Sr.",
         "nombre":"Javi",
         "apellidos":"Martin",
         "tipo_documento":"NIF",
         "numero_documento":"03920418E",
         "fecha_nacimiento":"1987-12-08",
         "email":"fjacevedo@intermundial.es",
         "direccion":"Paseo recoletos",
         "localidad":"Madrid",
         "provincia":"Madrid",
         "pais":{
            "codigo_pais": "ESP"
         },
         "codigo_postal":"28004",
         "telefono":612123456,
         "is_principal":true
      }
   ],
   "coberturas_ampliadas":[
    {
      "id_poliza": "16571",  
      "id_cruce": 200000039381,
      "tipo": "incluida-lista",
      "valores": [
        {
            "id": 2207,
            "prima": 14.96,
            "limite": 15000
        }
      ]
    },
    {
      "id_poliza": "16571", 
      "id_cruce": 200000039404,
      "tipo": "incluida-lista",
      "valores": [
        {
          "limite": 1200,
          "prima": 0
        }
      ]
    },
    {
      "id_poliza": "16571",   
      "id_cruce": 200000039412,
      "tipo": "incluida-lista",
      "valores": [
        {
          "id": 2247,
          "limite": 10000,
          "prima": 0
        }
      ]
    }
   ],
   "fecha_inicio":"2017-07-28",
   "fecha_fin":"2017-07-31",
   "total_asegurados":2,
   "parametros": [
       {
         "id_parametro": 41,
         "id_valor": 41
       },
       {
         "id_parametro": 42,
         "id_valor": 45
       }
   ]
}
```